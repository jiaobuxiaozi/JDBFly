/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect;

import java.lang.reflect.Method;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.constant.JKeyOrder;
import com.jianggujin.dbfly.mybatis.dialect.fn.JFnHandler;
import com.jianggujin.dbfly.mybatis.dialect.fn.JMySQLFnHandler;
import com.jianggujin.dbfly.mybatis.dialect.page.JAbstractPageSqlNode;
import com.jianggujin.dbfly.mybatis.dialect.page.JLimitPageSqlNode;
import com.jianggujin.dbfly.mybatis.entity.JEntity;

/**
 * 用于MySql的方言实现
 * 
 * @author jianggujin
 *
 */
public class JMySQLDialect extends JAbstarctDialect {

    private JInsertValuesHandler insertValuesHandler = new JInsertValuesHandler();
    private JMySQLFnHandler fnHandler = new JMySQLFnHandler();

    @Override
    public JIdentity getIdentity() {
        return new JIdentity("SELECT LAST_INSERT_ID()", JKeyOrder.AFTER);
    }

    @Override
    protected JFnHandler getFnHandler() {
        return fnHandler;
    }

    @Override
    protected JAbstractPageSqlNode handlePageNode(JXMLScriptBuilder builder, XNode nodeToHandle,
            MixedSqlNode mixedSqlNode, String name, String startRow, String endRow, String pageSize) {
        return new JLimitPageSqlNode(mixedSqlNode, name, startRow, endRow, pageSize);
    }

    @Override
    public boolean handleInsertListMapperMethod(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement, JEntity entity) {
        return insertValuesHandler.handleInsertList(configuration, mapperClass, method, document, mapperElement,
                entity);
    }

    @Override
    public String name() {
        return "MySQL";
    }
}
