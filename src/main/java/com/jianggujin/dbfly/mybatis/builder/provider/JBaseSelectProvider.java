/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.provider;

import java.lang.reflect.Method;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.util.JElementBuilder;
import com.jianggujin.dbfly.mybatis.util.JProviderHelper;

public class JBaseSelectProvider {
    /**
     * 查询
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectOne(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        String resultMap = JProviderHelper.checkAndCreateResultMap(configuration, document, mapperElement, entity);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, resultMap, null);

        JProviderHelper.selectAllColumns(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.whereAllIfColumns(document, selectElement, entity, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 根据Condition查询一个结果
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectOneByCondition(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement) {
        selectByCondition(configuration, mapperClass, method, document, mapperElement);
    }

    /**
     * 查询
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void select(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        String resultMap = JProviderHelper.checkAndCreateResultMap(configuration, document, mapperElement, entity);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, resultMap, null);
        JProviderHelper.selectAllColumns(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.whereAllIfColumns(document, selectElement, entity, null);
        JProviderHelper.orderByDefault(document, selectElement, entity);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 根据主键进行查询
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectById(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        String resultMap = JProviderHelper.checkAndCreateResultMap(configuration, document, mapperElement, entity);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, resultMap, null);

        JProviderHelper.selectAllColumns(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.wherePKColumns(document, selectElement, entity, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 根据Condition查询
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectByCondition(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        String resultMap = JProviderHelper.checkAndCreateResultMap(configuration, document, mapperElement, entity);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, resultMap, null);
        JElementBuilder.appendChild(document, selectElement, "SELECT ");
        Element ifElement = JElementBuilder.buildIfElement(document, "distinct");
        JElementBuilder.appendChild(document, ifElement, " DISTINCT ");
        selectElement.appendChild(ifElement);
        // 支持查询指定列
        JProviderHelper.conditionSelectColumns(document, selectElement, entity, null);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.conditionWhereClause(document, selectElement, null);
        JProviderHelper.conditionOrderBy(document, selectElement, entity, null);
        JProviderHelper.conditionForUpdate(document, selectElement, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 查询总数
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectCount(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, null,
                method.getReturnType().getCanonicalName());

        JProviderHelper.selectCount(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.whereAllIfColumns(document, selectElement, entity, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 查询总数
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectCountAll(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, null,
                method.getReturnType().getCanonicalName());

        JProviderHelper.selectCount(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 根据Condition查询总数
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectCountByCondition(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, null,
                method.getReturnType().getCanonicalName());
        JElementBuilder.appendChild(document, selectElement, "SELECT ");
        JProviderHelper.conditionCountColumn(document, selectElement, entity, null);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.conditionWhereClause(document, selectElement, null);
        JProviderHelper.conditionForUpdate(document, selectElement, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 查询总数
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void exists(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, null,
                method.getReturnType().getCanonicalName());

        JProviderHelper.selectCountExists(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.whereAllIfColumns(document, selectElement, entity, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 根据主键查询总数
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void existsById(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, null,
                method.getReturnType().getCanonicalName());

        JProviderHelper.selectCountExists(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.wherePKColumns(document, selectElement, entity, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 根据Condition查询总数
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void existsByCondition(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, null,
                method.getReturnType().getCanonicalName());

        JProviderHelper.selectCountExists(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.conditionWhereClause(document, selectElement, null);
        mapperElement.appendChild(selectElement);
    }

    /**
     * 查询全部结果
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void selectAll(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        String resultMap = JProviderHelper.checkAndCreateResultMap(configuration, document, mapperElement, entity);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, resultMap, null);
        JProviderHelper.selectAllColumns(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity, null);
        JProviderHelper.orderByDefault(document, selectElement, entity);
        mapperElement.appendChild(selectElement);
    }

}
