/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.provider;

import java.lang.reflect.Method;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.util.JElementBuilder;
import com.jianggujin.dbfly.mybatis.util.JProviderHelper;

public class JBaseDeleteProvider {

    /**
     * 通过条件删除
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void delete(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element deleteElement = JElementBuilder.buildDeleteElement(document, method);
        JProviderHelper.deleteFromTable(configuration, document, deleteElement, entity, null);
        JProviderHelper.whereAllIfColumns(document, deleteElement, entity, null);
        mapperElement.appendChild(deleteElement);
    }

    /**
     * 通过主键删除
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void deleteById(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element deleteElement = JElementBuilder.buildDeleteElement(document, method);
        JProviderHelper.deleteFromTable(configuration, document, deleteElement, entity, null);
        JProviderHelper.wherePKColumns(document, deleteElement, entity, null);
        mapperElement.appendChild(deleteElement);
    }

    /**
     * 根据Condition删除
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void deleteByCondition(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element deleteElement = JElementBuilder.buildDeleteElement(document, method);
        JProviderHelper.deleteFromTable(configuration, document, deleteElement, entity, null);
        JProviderHelper.conditionWhereClause(document, deleteElement, null);
        mapperElement.appendChild(deleteElement);
    }

}
