package com.jianggujin.dbfly.test.mapper;

import java.util.List;

import com.jianggujin.dbfly.mybatis.builder.mapper.JMethodMapper;
import com.jianggujin.dbfly.mybatis.builder.method.JOrderBy;
import com.jianggujin.dbfly.mybatis.builder.method.JOrderProperty;
import com.jianggujin.dbfly.mybatis.builder.method.JProperty;
import com.jianggujin.dbfly.mybatis.constant.JOrderStrategy;
import com.jianggujin.dbfly.test.entity.TestEntity;

public interface TestMethodMapper extends JMethodMapper<TestEntity> {

    List<TestEntity> findByIdNotNull();

    List<TestEntity> findByIdNull();

    List<TestEntity> findByIdNot(Integer id);

    TestEntity findById(Integer id);

    List<TestEntity> findByIdGreaterThan(Integer id);

    List<TestEntity> findByIdGreaterThanEqual(Integer id);

    List<TestEntity> findByIdLessThan(Integer id);

    List<TestEntity> findByIdLessThanEqual(Integer id);

    List<TestEntity> findByIdBefore(Integer id);

    List<TestEntity> findByIdBeforeEqual(Integer id);

    List<TestEntity> findByIdAfter(Integer id);

    List<TestEntity> findByIdAfterEqual(Integer id);

    List<TestEntity> findByIdNotIn(List<Integer> ids);

    List<TestEntity> findByIdIn(List<Integer> ids);

    List<TestEntity> findByIdBetween(Integer min, Integer max);

    List<TestEntity> findByIdNotBetween(Integer min, Integer max);

    List<TestEntity> findByCodeLike(String code);

    List<TestEntity> findByCodeNotLike(String code);

    int deleteById(Integer id);

    @JProperty(exclude = { "id" })
    @JOrderBy(value = { @JOrderProperty(value = "code", strategy = JOrderStrategy.DESC) })
    List<TestEntity> findAll();

    String selectCodeById(Integer id);

    List<String> selectCodeBy();
}
