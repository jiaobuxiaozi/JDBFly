package com.jianggujin.dbfly.test.mapper;

import com.jianggujin.dbfly.mybatis.builder.mapper.JBaseSelectMapper;
import com.jianggujin.dbfly.test.entity.TestDynamicTableEntity;

public interface TestDynamicTableEntityMapper extends JBaseSelectMapper<TestDynamicTableEntity> {
}
