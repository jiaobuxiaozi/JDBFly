/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.value;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.ibatis.reflection.MetaObject;

import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.entity.JEntityColumn;
import com.jianggujin.dbfly.mybatis.entity.JEntityResolver;
import com.jianggujin.dbfly.mybatis.util.JMetaObjectUtils;
import com.jianggujin.dbfly.util.JDBFlyException;

/**
 * Value生成工具
 * 
 * @author jianggujin
 *
 */
public class JValueGeneratorUtils {

    /**
     * 缓存
     */
    private static final Map<Class<? extends JValueGenerator>, JValueGenerator> CACHE = new ConcurrentHashMap<Class<? extends JValueGenerator>, JValueGenerator>();

    private static final ReentrantLock LOCK = new ReentrantLock();

    /**
     * 生成Value
     *
     * @param target
     * @param entityClass
     * @param property
     * @throws JDBFlyException
     */
    public static void generateValue(Object target, Class<?> entityClass, String property) throws JDBFlyException {
        JEntity entity = JEntityResolver.getEntity(entityClass);
        JEntityColumn column = entity.getColumn(property);
        if (column == null) {
            return;
        }
        Class<? extends JValueGenerator> valueGeneratorClass = column.getValueGeneratorClass();
        try {
            JValueGenerator valueGenerator;
            if (CACHE.containsKey(valueGeneratorClass)) {
                valueGenerator = CACHE.get(valueGeneratorClass);
            } else {
                LOCK.lock();
                try {
                    if (!CACHE.containsKey(valueGeneratorClass)) {
                        CACHE.put(valueGeneratorClass, valueGeneratorClass.newInstance());
                    }
                    valueGenerator = CACHE.get(valueGeneratorClass);
                } finally {
                    LOCK.unlock();
                }
            }
            MetaObject metaObject = JMetaObjectUtils.forObject(target);
            if (metaObject.getValue(property) == null) {
                metaObject.setValue(property, valueGenerator.generateValue(entity, column));
            }
        } catch (Exception e) {
            throw new JDBFlyException("生成Value失败", e);
        }
    }

    /**
     * 生成多个Value
     *
     * @param target
     * @param entityClass
     * @param propertiesJoin
     * @throws JDBFlyException
     */
    public static void generateValues(Object target, Class<?> entityClass, String propertiesJoin)
            throws JDBFlyException {
        JEntity entity = JEntityResolver.getEntity(entityClass);
        String[] properties = propertiesJoin.split(",");
        try {
            for (String property : properties) {
                JEntityColumn column = entity.getColumn(property);
                if (column == null) {
                    return;
                }
                Class<? extends JValueGenerator> valueGeneratorClass = column.getValueGeneratorClass();
                JValueGenerator valueGenerator;
                if (CACHE.containsKey(valueGeneratorClass)) {
                    valueGenerator = CACHE.get(valueGeneratorClass);
                } else {
                    LOCK.lock();
                    try {
                        if (!CACHE.containsKey(valueGeneratorClass)) {
                            CACHE.put(valueGeneratorClass, valueGeneratorClass.newInstance());
                        }
                        valueGenerator = CACHE.get(valueGeneratorClass);
                    } finally {
                        LOCK.unlock();
                    }
                }
                MetaObject metaObject = JMetaObjectUtils.forObject(target);
                if (metaObject.getValue(property) == null) {
                    metaObject.setValue(property, valueGenerator.generateValue(entity, column));
                }
            }
        } catch (Exception e) {
            throw new JDBFlyException("生成多个Value失败", e);
        }
    }

}
