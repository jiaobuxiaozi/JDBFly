package com.jianggujin.dbfly.test.mapper;

import com.jianggujin.dbfly.mybatis.builder.mapper.JBaseInsertMapper;
import com.jianggujin.dbfly.test.entity.TestEntityAuto;

public interface TestEntityAutoMapper extends JBaseInsertMapper<TestEntityAuto> {
}
