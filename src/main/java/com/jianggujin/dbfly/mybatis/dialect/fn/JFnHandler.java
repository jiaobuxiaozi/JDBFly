/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.fn;

import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;

import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;

/**
 * 默认扩展的函数处理器
 * 
 * @author jianggujin
 *
 */
public interface JFnHandler {
    /****** 字符串函数 ******/
    /**
     * 处理ascii，返回参数第一个字符的ASCII码
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnAsciiNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理concat，字符串拼接
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     * @return
     */
    boolean handleFnConcatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理char_length，返回字符串的字符数
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnCharLengthNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理bit_length，返回字符串的字节数
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnBitLengthNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理position，返回指定的子字串的位置
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnPositionNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理replace，把字串str里出现地所有子字串from替换成子字串to
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnReplaceNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理sub_str，从字符串str的start位置截取长度为length的子字符串
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnSubStrNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理upper，将字符串转换为大写
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnUpperNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理lower，将字符串转换为小写
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnLowerNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);
    /**
     * 处理trim，去掉字符串开始和结尾处的空格
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnTrimNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);
    /**
     * 处理ltrim，去掉字符串开始处的空格
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnLTrimNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);
    /**
     * 处理rtrim， 去掉字符串结尾处的空格
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnRTrimNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /****** 数字函数 ******/
    /****** 日期函数 ******/
    /****** 类型转换函数 ******/
    /**
     * 处理date_format，将日期转换为字符串
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnDateFormatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理str_to_date，将字符串转换为日期
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnStrToDateNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /****** 其他函数 ******/
    /**
     * 处理nvl
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    boolean handleFnNvlNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

}
