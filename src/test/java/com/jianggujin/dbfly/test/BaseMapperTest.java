package com.jianggujin.dbfly.test;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JSqlSessionFactoryBuilder;
import com.jianggujin.dbfly.mybatis.entity.JCondition;
import com.jianggujin.dbfly.mybatis.entity.JLambdaCondition;
import com.jianggujin.dbfly.test.entity.TestEntity;
import com.jianggujin.dbfly.test.mapper.TestEntityMapper;

public class BaseMapperTest {
    private SqlSession session = null;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
        session = sqlSessionFactory.openSession();
    }

    @Test
    public void insert() {
        TestEntity record = new TestEntity();
        record.setId(1);
        record.setCode("100");
        record.setVersion(12);
        assertEquals(1, session.getMapper(TestEntityMapper.class).insert(record));
    }

    @Test
    public void insertSelective() {
        TestEntity record = new TestEntity();
        record.setId(2);
        record.setCode("100");
        assertEquals(1, session.getMapper(TestEntityMapper.class).insertSelective(record));
    }

    @Test
    public void updateById() {
        TestEntity record = new TestEntity();
        record.setId(1);
        record.setCode("200");
        record.setVersion(1);
        assertEquals(1, session.getMapper(TestEntityMapper.class).updateById(record));
    }

    @Test
    public void updateSelectiveById() {
        TestEntity record = new TestEntity();
        record.setId(1);
        record.setCode("300");
        assertEquals(1, session.getMapper(TestEntityMapper.class).updateSelectiveById(record));
    }

    @Test
    public void updateByCondition() {
        TestEntity record = new TestEntity();
        record.setCode("200");
        record.setVersion(1);
        JCondition<TestEntity> condition = new JCondition<TestEntity>(TestEntity.class);
        condition.and().andEqualTo("id", 1);
        assertEquals(1, session.getMapper(TestEntityMapper.class).updateByCondition(record, condition));
    }

    @Test
    public void updateSelectiveByCondition() {
        TestEntity record = new TestEntity();
        record.setCode("200");
        JCondition<TestEntity> condition = new JCondition<TestEntity>(TestEntity.class);
        condition.and().andEqualTo("id", 1);
        assertEquals(1, session.getMapper(TestEntityMapper.class).updateSelectiveByCondition(record, condition));
    }

    @Test
    public void updateByLambdaCondition() {
        TestEntity record = new TestEntity();
        record.setCode("200");
        record.setVersion(1);
        JLambdaCondition<TestEntity> condition = new JLambdaCondition<>(TestEntity.class);
        condition.lambdaAnd().andEqualTo(TestEntity::getId, 1);
        assertEquals(1, session.getMapper(TestEntityMapper.class).updateByCondition(record, condition));
    }

    @Test
    public void updateSelectiveByLambdaCondition() {
        TestEntity record = new TestEntity();
        record.setCode("200");
        JLambdaCondition<TestEntity> condition = new JLambdaCondition<>(TestEntity.class);
        condition.lambdaAnd().andEqualTo(TestEntity::getId, 1);
        assertEquals(1, session.getMapper(TestEntityMapper.class).updateSelectiveByCondition(record, condition));
    }

    @Test
    public void selectOne() {
        TestEntity record = new TestEntity();
        record.setId(1);
        System.err.println(session.getMapper(TestEntityMapper.class).selectOne(record));
    }

    @Test
    public void selectOneByCondition() {
        JCondition<TestEntity> condition = new JCondition<TestEntity>(TestEntity.class);
        condition.and().andEqualTo("id", 1);
        System.err.println(session.getMapper(TestEntityMapper.class).selectOneByCondition(condition));
    }

    @Test
    public void select() {
        TestEntity record = new TestEntity();
        record.setId(1);
        System.err.println(session.getMapper(TestEntityMapper.class).select(record));
    }

    @Test
    public void selectById() {
        System.err.println(session.getMapper(TestEntityMapper.class).selectById(1));
    }

    @Test
    public void selectByCondition() {
        JLambdaCondition<TestEntity> condition = new JLambdaCondition<>(TestEntity.class);
        condition.selectProperties("id", "code");
        condition.lambdaAnd().eq(TestEntity::getId, 1);
        // condition.and().andEqualTo("id", 1);
        // condition.orderBy("id").asc().orderBy("code").desc();
        System.err.println(session.getMapper(TestEntityMapper.class).selectByCondition(condition));
    }

    @Test
    public void selectCount() {
        TestEntity record = new TestEntity();
        record.setId(1);
        System.err.println(session.getMapper(TestEntityMapper.class).selectCount(record));
    }

    @Test
    public void selectCountByCondition() {
        JCondition<TestEntity> condition = new JCondition<TestEntity>(TestEntity.class);
        condition.and().andEqualTo("id", 1);
        System.err.println(session.getMapper(TestEntityMapper.class).selectCountByCondition(condition));
    }

    @Test
    public void existsById() {
        System.err.println(session.getMapper(TestEntityMapper.class).existsById(1));
    }

    @Test
    public void existsByCondition() {
        JCondition<TestEntity> condition = new JCondition<TestEntity>(TestEntity.class);
        condition.and().andEqualTo("id", 1);
        System.err.println(session.getMapper(TestEntityMapper.class).existsByCondition(condition));
    }

    @Test
    public void selectAll() {
        System.err.println(session.getMapper(TestEntityMapper.class).selectAll());
    }

    @Test
    public void exists() {
        TestEntity record = new TestEntity();
        record.setId(1);
        System.err.println(session.getMapper(TestEntityMapper.class).exists(record));
    }

    @Test
    public void delete() {
        TestEntity record = new TestEntity();
        record.setId(1);
        assertEquals(1, session.getMapper(TestEntityMapper.class).delete(record));
    }

    @Test
    public void deleteById() {
        assertEquals(1, session.getMapper(TestEntityMapper.class).deleteById(6));
    }

    @Test
    public void deleteByCondition() {
        JCondition<TestEntity> condition = new JCondition<TestEntity>(TestEntity.class);
        condition.and().andEqualTo("id", 5);
        assertEquals(1, session.getMapper(TestEntityMapper.class).deleteByCondition(condition));
    }

    @After
    public void destory() {
        if (session != null) {
            session.close();
        }
    }
}
