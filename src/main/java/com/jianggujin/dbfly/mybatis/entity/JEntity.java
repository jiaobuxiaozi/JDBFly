/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import java.util.List;
import java.util.Map;

import com.jianggujin.dbfly.mybatis.table.JTableNameGenerator;
import com.jianggujin.dbfly.util.JStringUtils;

/**
 * 实体信息
 * 
 * @author jianggujin
 *
 */
public class JEntity {
    /**
     * 表名称
     */
    private String tableName;
    /**
     * schema
     */
    private String schema;
    /**
     * 完整表名称
     */
    private String fullTableName;
    /**
     * 全部属性列
     */
    private List<JEntityColumn> columns;
    /**
     * 主键属性列
     */
    private List<JEntityIdColumn> idColumns;
    /**
     * 支持版本的属性列
     */
    private JEntityColumn versionColumn;
    /**
     * 实体类
     */
    private final Class<?> entityClass;
    /**
     * 排序SQL语句
     */
    private String orderByClause;
    /**
     * 属性映射
     */
    private Map<String, JEntityColumn> propertyMap;
    /**
     * 动态表名实现类
     */
    private Class<? extends JTableNameGenerator> tableNameGeneratorClass;

    public JEntity(Class<?> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * 设置表名
     * 
     * @param
     */
    protected void setTableName(String tableName) {
        this.tableName = tableName;
        this.setFullTableName();
    }

    /**
     * 设置Schema
     * 
     * @param
     */
    protected void setSchema(String schema) {
        this.schema = schema;
        this.setFullTableName();
    }

    private void setFullTableName() {
        if (JStringUtils.isNotEmpty(getSchema())) {
            this.fullTableName = JStringUtils.format("{}.{}", getSchema(), getTableName());
        }
        this.fullTableName = getTableName();
    }

    /**
     * 设置属性列
     * 
     * @param
     */
    protected void setColumns(List<JEntityColumn> columns) {
        this.columns = columns;
    }

    /**
     * 设置ID属性列
     * 
     * @param
     */
    protected void setIdColumns(List<JEntityIdColumn> idColumns) {
        this.idColumns = idColumns;
    }

    /**
     * 设置排序SQL语句
     * 
     * @param orderByClause
     */
    protected void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * 设置属性映射
     * 
     * @param propertyMap
     */
    protected void setPropertyMap(Map<String, JEntityColumn> propertyMap) {
        this.propertyMap = propertyMap;
    }

    /**
     * 获得表名
     * 
     * @return
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * 获得schema
     * 
     * @return
     */
    public String getSchema() {
        return schema;
    }

    /**
     * 获得完整表名
     * 
     * @return
     */
    public String getFullTableName() {
        return fullTableName;
    }

    /**
     * 获得全部属性列
     * 
     * @return
     */
    public List<JEntityColumn> getColumns() {
        return columns;
    }

    /**
     * 获得主键属性列
     * 
     * @return
     */
    public List<JEntityIdColumn> getIdColumns() {
        return idColumns;
    }

    /**
     * 是否存在主键
     * 
     * @return
     */
    public boolean hasId() {
        return this.idColumns != null && !this.idColumns.isEmpty();
    }

    /**
     * 获得实体类
     * 
     * @return
     */
    public Class<?> getEntityClass() {
        return entityClass;
    }

    /**
     * 获得排序SQL语句
     * 
     * @return
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * 获得属性映射
     * 
     * @return
     */
    public Map<String, JEntityColumn> getPropertyMap() {
        return propertyMap;
    }

    /**
     * 设置动态表名实现类
     * 
     * @param tableNameGeneratorClass
     */
    protected void setTableNameGeneratorClass(Class<? extends JTableNameGenerator> tableNameGeneratorClass) {
        this.tableNameGeneratorClass = tableNameGeneratorClass;
    }

    /**
     * 获得动态表名实现类
     * 
     * @return
     */
    public Class<? extends JTableNameGenerator> getTableNameGeneratorClass() {
        return tableNameGeneratorClass;
    }

    /**
     * 是否使用动态表名
     * 
     * @return
     */
    public boolean useDynamicTableName() {
        return this.tableNameGeneratorClass != null;
    }

    /**
     * 是否存在指定属性
     * 
     * @param property
     * @return
     */
    public boolean hasProperty(String property) {
        return this.propertyMap.containsKey(property);
    }

    /**
     * 获得指定属性列信息
     * 
     * @param property
     * @return
     */
    public JEntityColumn getColumn(String property) {
        return this.propertyMap.get(property);
    }

    /**
     * 支持版本的属性列
     * 
     * @return
     */
    public JEntityColumn getVersionColumn() {
        return versionColumn;
    }

    /**
     * 支持版本的属性列
     * 
     * @param versionColumn
     */
    protected void setVersionColumn(JEntityColumn versionColumn) {
        this.versionColumn = versionColumn;
    }

    /**
     * 是否存在支持版本的属性列
     * 
     * @return
     */
    public boolean hasVersion() {
        return this.versionColumn != null;
    }
}
