/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.util;

import java.lang.reflect.Method;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.entity.JEntityIdColumn;
import com.jianggujin.dbfly.util.JStringUtils;

public class JElementBuilder {
    public static Element appendChild(Document document, Element parentElement, CharSequence content) {
        parentElement.appendChild(document.createTextNode(content.toString()));
        return parentElement;
    }

    public static Element buildElement(Document document, String tagName, String... attrs) {
        Element element = document.createElement(tagName);
        return setAttributes(element, attrs);
    }

    public static Element setAttributes(Element element, String... attrs) {
        int len = attrs.length;
        for (int i = 0; i < len; i++) {
            String name = attrs[i];
            String value = attrs[++i];
            if (JStringUtils.isNotEmpty(value)) {
                element.setAttribute(name, value);
            }
        }
        return element;
    }

    public static Element buildSelectElement(Document document, Method method, String resultMap, String resultType) {
        return buildSelectElement(document, method.getName(), resultMap, resultType);
    }

    public static Element buildSelectElement(Document document, String id, String resultMap, String resultType) {
        Element selectElement = buildElement(document, "select", "id", id);
        if (resultMap != null) {
            selectElement.setAttribute("resultMap", resultMap);
        }
        if (resultType != null) {
            selectElement.setAttribute("resultType", resultType);
        }
        return selectElement;
    }

    public static Element buildDeleteElement(Document document, Method method) {
        return buildDeleteElement(document, method.getName());
    }

    public static Element buildDeleteElement(Document document, String id) {
        return buildElement(document, "delete", "id", id);
    }

    public static Element buildUpdateElement(Document document, Method method) {
        return buildUpdateElement(document, method.getName());
    }

    public static Element buildUpdateElement(Document document, String id) {
        return buildElement(document, "update", "id", id);
    }

    public static Element buildInsertElement(Document document, Method method) {
        return buildInsertElement(document, method.getName());
    }

    public static Element buildInsertElement(Document document, String id) {
        return buildElement(document, "insert", "id", id);
    }

    public static Element buildResultMapElement(Document document, String id, Class<?> entityClass) {
        return buildResultMapElement(document, id, entityClass.getCanonicalName());
    }

    public static Element buildResultMapElement(Document document, String id, String type) {
        return buildElement(document, "resultMap", "id", id, "type", type);
    }

    public static Element buildWhereElement(Document document) {
        return buildElement(document, "where");
    }

    public static Element buildIfElement(Document document) {
        return buildElement(document, "if");
    }

    public static Element buildIfElement(Document document, String test) {
        return buildElement(document, "if", "test", test);
    }

    public static Element buildSelectKeyElement(Document document, JEntityIdColumn idColumn) {
        return buildSelectKeyElement(document, idColumn.getProperty(), idColumn.getKeyOrder().name(),
                idColumn.getJavaType().getCanonicalName());
    }

    public static Element buildSelectKeyElement(Document document, String keyProperty, String order,
            String resultType) {
        return buildElement(document, "selectKey", "keyProperty", keyProperty, "order", order, "resultType",
                resultType);
    }

    public static Element buildBindElement(Document document, String name, String value) {
        return buildElement(document, "bind", "name", name, "value", value);
    }

    public static Element buildTrimElement(Document document, String prefix, String suffix) {
        return buildElement(document, "trim", "prefix", prefix, "suffix", suffix);
    }

    public static Element buildSetElement(Document document) {
        return buildElement(document, "set");
    }

    public static Element buildChooseElement(Document document) {
        return buildElement(document, "choose");
    }

    public static Element buildWhenElement(Document document, String test) {
        return buildElement(document, "when", "test", test);
    }

    public static Element buildOtherwiseElement(Document document) {
        return buildElement(document, "otherwise");
    }

    public static Element buildForeachElement(Document document, String collection, String item) {
        return buildElement(document, "foreach", "collection", collection, "item", item);
    }

    public static Element buildForeachElement(Document document, String collection, String item, String separator) {
        return buildElement(document, "foreach", "collection", collection, "item", item, "separator", separator);
    }

    public static Element buildForeachElement(Document document, String open, String close, String collection,
            String item, String separator) {
        return buildElement(document, "foreach", "open", open, "close", close, "collection", collection, "item", item,
                "separator", separator);
    }
}
