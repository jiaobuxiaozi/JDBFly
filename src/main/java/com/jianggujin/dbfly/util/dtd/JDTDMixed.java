/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util.dtd;

import java.io.IOException;
import java.io.PrintWriter;

public class JDTDMixed extends JDTDContainer {
    public JDTDMixed(JDTDMixed mixed) {
        super(mixed.getCardinal(), mixed.items.toArray(new JDTDItem[0]));
    }

    public JDTDMixed(JDTDItem... items) {
        super(items);
    }

    public JDTDMixed(String... values) {
        super(values);
    }

    public JDTDMixed(JDTDCardinal cardinal, JDTDItem... items) {
        super(cardinal, items);
    }

    public JDTDMixed(JDTDCardinal cardinal, JDTDCardinal itemCardinal, String... values) {
        super(cardinal, itemCardinal, values);
    }

    @Override
    public void writeInternal(PrintWriter out) throws IOException {
        out.print("(");
        boolean isFirst = true;
        for (JDTDItem item : this.items) {
            if (!isFirst) {
                out.print(" | ");
            }
            isFirst = false;
            item.write(out);
        }
        out.print(")");
    }
}
