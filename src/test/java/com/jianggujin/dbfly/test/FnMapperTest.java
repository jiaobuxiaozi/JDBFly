package com.jianggujin.dbfly.test;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JSqlSessionFactoryBuilder;
import com.jianggujin.dbfly.mybatis.entity.JPage;
import com.jianggujin.dbfly.test.mapper.FnMapper;

public class FnMapperTest {
    private SqlSession session = null;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
        session = sqlSessionFactory.openSession();
    }

    @Test
    public void selectNow() {
        System.err.println(session.getMapper(FnMapper.class).selectNow1());
    }

    @Test
    public void selectNow2() {
        System.err.println(session.getMapper(FnMapper.class).selectNow2());
    }

    @Test
    public void selectNow3() {
        System.err.println(session.getMapper(FnMapper.class).selectNow3());
    }

    @Test
    public void selectNow4() {
        System.err.println(session.getMapper(FnMapper.class).selectNow4());
    }

    @Test
    public void testNull() {
        System.err.println(session.getMapper(FnMapper.class).testNull());
    }

    @Test
    public void testConcat() {
        System.err.println(session.getMapper(FnMapper.class).testConcat());
    }

    @Test
    public void selectPage() {
        JPage page = new JPage(1, 15);
        System.err.println(session.getMapper(FnMapper.class).selectPage(page));
    }

    @After
    public void destory() {
        if (session != null) {
            session.close();
        }
    }
}
