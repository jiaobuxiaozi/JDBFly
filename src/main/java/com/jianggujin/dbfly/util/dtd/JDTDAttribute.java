/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util.dtd;

import java.io.IOException;
import java.io.PrintWriter;

import com.jianggujin.dbfly.util.JAssert;

/**
 * 元素属性
 * 
 * @author jianggujin
 *
 */
public class JDTDAttribute implements JDTDOutput {
    /**
     * 属性名称
     */
    public final String name;

    /**
     * 元素类型
     */
    public final Object type;

    /**
     * 元素属性值
     */
    public final JDTDDecl decl;

    /**
     * 默认值
     */
    public final String defaultValue;

    /**
     * 构建属性不是必须的CDATA属性
     * 
     * @param name
     */
    public JDTDAttribute(String name) {
        this(name, "CDATA", JDTDDecl.IMPLIED, null);
    }

    /**
     * 构建指定属性值格式的CDATA属性
     * 
     * @param name
     * @param decl
     */
    public JDTDAttribute(String name, JDTDDecl decl) {
        this(name, "CDATA", decl, null);
    }

    public JDTDAttribute(String name, String type) {
        this(name, type, JDTDDecl.IMPLIED, null);
    }

    public JDTDAttribute(String name, JDTDEnumeration type) {
        this(name, type, JDTDDecl.IMPLIED, null);
    }

    public JDTDAttribute(String name, String type, JDTDDecl decl) {
        this(name, type, decl, null);
    }

    public JDTDAttribute(String name, JDTDEnumeration type, JDTDDecl decl) {
        this(name, type, decl, null);
    }

    public JDTDAttribute(String name, String type, JDTDDecl decl, String defaultValue) {
        this(name, type, decl, defaultValue, true);
    }

    public JDTDAttribute(String name, JDTDEnumeration type, JDTDDecl decl, String defaultValue) {
        this(name, type, decl, defaultValue, true);
    }

    private JDTDAttribute(String name, Object type, JDTDDecl decl, String defaultValue, boolean b) {
        JAssert.notNull(name, "name不能为空");
        this.name = name;
        this.type = type;
        this.decl = decl;
        this.defaultValue = defaultValue;
    }

    @Override
    public void write(PrintWriter out) throws IOException {
        out.print(this.name);
        out.print(" ");
        if (this.type instanceof String) {
            out.print(this.type);
        } else if (this.type instanceof JDTDEnumeration) {
            ((JDTDEnumeration) this.type).write(out);
        }
        if (this.decl != null) {
            this.decl.write(out);
        }
        if (this.defaultValue != null) {
            out.print(" \"");
            out.print(this.defaultValue);
            out.print("\"");
        }
    }

    public String getName() {
        return name;
    }

    public Object getType() {
        return type;
    }

    public JDTDDecl getDecl() {
        return decl;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

}
