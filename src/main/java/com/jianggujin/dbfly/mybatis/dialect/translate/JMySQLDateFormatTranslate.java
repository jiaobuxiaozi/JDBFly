/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.translate;

/**
 * MySQL日期格式化翻译
 * 
 * @author jianggujin
 *
 */
public class JMySQLDateFormatTranslate implements JFormatTranslate {
    @Override
    public void translate(StringBuilder builder, String format) {
        int len = format.length();
        for (int i = 0; i < len; i++) {
            char c = format.charAt(i);
            switch (c) {
            case 'y':
                if (len > i + 3) {
                    if ('y' == format.charAt(i + 1) && 'y' == format.charAt(i + 2) && 'y' == format.charAt(i + 3)) {
                        builder.append("%Y");
                        i += 3;
                        break;
                    }
                }
                if (len > i + 1) {
                    if ('y' == format.charAt(i + 1)) {
                        builder.append("%y");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            case 'M':
                if (len > i + 1) {
                    if ('M' == format.charAt(i + 1)) {
                        builder.append("%m");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            case 'd':
                if (len > i + 1) {
                    if ('d' == format.charAt(i + 1)) {
                        builder.append("%d");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            case 'H':
                if (len > i + 1) {
                    if ('H' == format.charAt(i + 1)) {
                        builder.append("%k");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            case 'h':
                if (len > i + 1) {
                    if ('h' == format.charAt(i + 1)) {
                        builder.append("%h");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            case 'm':
                if (len > i + 1) {
                    if ('m' == format.charAt(i + 1)) {
                        builder.append("%i");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            case 's':
                if (len > i + 1) {
                    if ('s' == format.charAt(i + 1)) {
                        builder.append("%s");
                        i += 1;
                        break;
                    }
                }
                builder.append(c);
                break;
            default:
                builder.append(c);
                break;
            }
        }
    }
}
