package com.jianggujin.dbfly.test.mapper;

import com.jianggujin.dbfly.mybatis.builder.mapper.JBaseMapper;
import com.jianggujin.dbfly.test.entity.TestEntity;

public interface TestEntityMapper extends JBaseMapper<TestEntity> {
}
