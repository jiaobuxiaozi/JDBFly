package com.jianggujin.dbfly.test;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JSqlSessionFactoryBuilder;
import com.jianggujin.dbfly.test.entity.TestDynamicTableEntity;
import com.jianggujin.dbfly.test.mapper.TestDynamicTableEntityMapper;

public class DynamicTableTest {
    private SqlSession session = null;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
        session = sqlSessionFactory.openSession();
    }

    @Test
    public void select() {
        TestDynamicTableEntity record = new TestDynamicTableEntity();
        record.setId(1);
        System.err.println(session.getMapper(TestDynamicTableEntityMapper.class).select(record));
        record.setId(6);
        System.err.println(session.getMapper(TestDynamicTableEntityMapper.class).select(record));
    }
}
