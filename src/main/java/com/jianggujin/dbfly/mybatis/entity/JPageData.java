/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import java.util.List;

/**
 * 分页数据
 * 
 * @author jianggujin
 *
 * @param <T>
 */
public class JPageData<T> extends JPage {

    /**
     * 当前数量
     */
    private int size;

    private List<T> data;

    public JPageData(JPage page) {
        super(page);
    }

    public JPageData(int pageNum, int pageSize) {
        super(pageNum, pageSize);
    }

    public JPageData(int pageNum, int pageSize, long total) {
        super(pageNum, pageSize, total);
    }

    public void setData(List<T> data) {
        this.data = data;
        this.size = data != null ? data.size() : 0;
    }

    public int getSize() {
        return size;
    }

    public List<T> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "JPageData [size=" + size + ", data=" + data + ", toString()=" + super.toString() + "]";
    }

}
