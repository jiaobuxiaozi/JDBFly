/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.table;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.entity.JEntity;

/**
 * 动态表名
 * 
 * @author jianggujin
 *
 */
public interface JTableNameGenerator {
    /**
     * 动态表名
     * 
     * @param configuration
     * @param document
     * @param parentElement
     * @param entity
     * @param parameterName
     */
    void generateTableName(JConfiguration configuration, Document document, Element parentElement, JEntity entity,
            String parameterName);

}
