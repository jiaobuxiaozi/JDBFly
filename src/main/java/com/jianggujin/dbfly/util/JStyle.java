/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util;

/**
 * 字段转换方式
 * 
 * @author jianggujin
 *
 */
public enum JStyle {
    /**
     * 原值
     */
    normal,
    /**
     * 驼峰转下划线
     */
    camelhump,
    /**
     * 转换为大写
     */
    uppercase,
    /**
     * 转换为小写
     */
    lowercase,
    /**
     * 驼峰转下划线大写形式
     */
    camelhumpAndUppercase,
    /**
     * 驼峰转下划线小写形式
     */
    camelhumpAndLowercase
}
