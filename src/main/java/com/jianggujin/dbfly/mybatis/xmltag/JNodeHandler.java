/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.xmltag;

import java.util.List;
import java.util.function.Consumer;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;

import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.util.dtd.JDTD;

/**
 * 节点处理器
 * 
 * @author jianggujin
 *
 */
public interface JNodeHandler extends Consumer<JDTD> {
    /**
     * Mybatis内置可扩展子元素的元素
     */
    String[] MYBATIS_INNER_ELEMENTS = { "select", "insert", "selectKey", "update", "delete", "sql", "trim", "where",
            "set", "foreach", "when", "otherwise", "if" };

    void handleNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    @Override
    default void accept(JDTD dtd) {
    }
}
