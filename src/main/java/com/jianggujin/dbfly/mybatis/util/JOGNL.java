/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.util;

import com.jianggujin.dbfly.mybatis.entity.JCondition;
import com.jianggujin.dbfly.mybatis.entity.JCriteria;
import com.jianggujin.dbfly.mybatis.entity.JCriterion;
import com.jianggujin.dbfly.util.JStringUtils;

public class JOGNL {

    /**
     * 是否包含自定义查询列
     *
     * @param parameter
     * @return
     */
    public static boolean hasSelectColumns(Object parameter) {
        if (parameter != null && parameter instanceof JCondition) {
            JCondition<?> condition = (JCondition<?>) parameter;
            if (condition.getSelectColumns() != null && condition.getSelectColumns().size() > 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否包含自定义 Count 列
     *
     * @param parameter
     * @return
     */
    public static boolean hasCountColumn(Object parameter) {
        if (parameter != null && parameter instanceof JCondition) {
            JCondition<?> condition = (JCondition<?>) parameter;
            return JStringUtils.isNotEmpty(condition.getCountColumn());
        }
        return false;
    }

    /**
     * 判断条件是 and 还是 or
     *
     * @param parameter
     * @return
     */
    public static String andOr(Object parameter) {
        if (parameter instanceof JCriteria) {
            return ((JCriteria) parameter).getAndOr();
        } else if (parameter instanceof JCriterion) {
            return ((JCriterion) parameter).getAndOr();
        } else if (parameter.getClass().getCanonicalName().endsWith("Criteria")) {
            return "or";
        } else {
            return "and";
        }
    }

    /**
     * 是否包含 forUpdate
     *
     * @param parameter
     * @return
     */
    public static boolean hasForUpdate(Object parameter) {
        if (parameter != null && parameter instanceof JCondition) {
            JCondition<?> condition = (JCondition<?>) parameter;
            return condition.isForUpdate();
        }
        return false;
    }
}
