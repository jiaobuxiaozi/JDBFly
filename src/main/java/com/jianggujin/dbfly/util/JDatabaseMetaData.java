/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * 数据库元数据
 * 
 * @author jianggujin
 *
 */
public class JDatabaseMetaData {
    private final String databaseProductName;
    private final String databaseProductVersion;
    private final String driverName;
    private final String driverVersion;
    private final int driverMajorVersion;
    private final int driverMinorVersion;

    public JDatabaseMetaData(DatabaseMetaData databaseMetaData) throws SQLException {
        this.databaseProductName = databaseMetaData.getDatabaseProductName();
        this.databaseProductVersion = databaseMetaData.getDatabaseProductVersion();
        this.driverName = databaseMetaData.getDriverName();
        this.driverVersion = databaseMetaData.getDriverVersion();
        this.driverMajorVersion = databaseMetaData.getDriverMajorVersion();
        this.driverMinorVersion = databaseMetaData.getDriverMinorVersion();
    }

    public String getDatabaseProductName() {
        return databaseProductName;
    }

    public String getDatabaseProductVersion() {
        return databaseProductVersion;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getDriverVersion() {
        return driverVersion;
    }

    public int getDriverMajorVersion() {
        return driverMajorVersion;
    }

    public int getDriverMinorVersion() {
        return driverMinorVersion;
    }

    @Override
    public String toString() {
        return "[databaseProductName=" + databaseProductName + ", databaseProductVersion=" + databaseProductVersion
                + ", driverName=" + driverName + ", driverVersion=" + driverVersion + ", driverMajorVersion="
                + driverMajorVersion + ", driverMinorVersion=" + driverMinorVersion + "]";
    }

}
