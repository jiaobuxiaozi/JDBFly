/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect;

import java.lang.reflect.Method;
import java.sql.DatabaseMetaData;
import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.builder.JParamConsts;
import com.jianggujin.dbfly.mybatis.dialect.fn.JFnHandler;
import com.jianggujin.dbfly.mybatis.dialect.page.JAbstractPageSqlNode;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.entity.JEntityColumn;
import com.jianggujin.dbfly.mybatis.util.JElementBuilder;
import com.jianggujin.dbfly.mybatis.util.JNodeUtils;
import com.jianggujin.dbfly.mybatis.util.JProviderHelper;
import com.jianggujin.dbfly.util.JBeanUtils;
import com.jianggujin.dbfly.util.JDatabaseMetaData;
import com.jianggujin.dbfly.util.JStringUtils;

/**
 * 方言抽象实现
 * 
 * @author jianggujin
 *
 */
public abstract class JAbstarctDialect implements JDialect {

    @Override
    public JIdentity getIdentity() {
        return null;
    }

    @Override
    public boolean handleFnNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        // 去除前三位"fn_"
        String fnName = nodeToHandle.getName().substring(3);
        return this.handleFnNode(fnName, builder, nodeToHandle, targetContents);
    }

    /**
     * 处理函数节点，根据函数名称匹配处理方法，方法签名：{@code void handleFn[函数名称下划线转首字母大写驼峰]Node(JXMLScriptBuilder
     * builder, XNode nodeToHandle, List<SqlNode> targetContents)}
     * 
     * @param fnName
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     */
    protected boolean handleFnNode(String fnName, JXMLScriptBuilder builder, XNode nodeToHandle,
            List<SqlNode> targetContents) {
        JFnHandler handler = getFnHandler();
        if (handler == null) {
            return false;
        }
        Method method = JBeanUtils.findMethod(handler.getClass(),
                JStringUtils.format("handleFn{}Node",
                        JStringUtils.firstCharToUpperCase(JStringUtils.underlineToCamelhump(fnName))),
                JXMLScriptBuilder.class, XNode.class, List.class);
        if (method == null) {
            return false;
        }
        return (boolean) JBeanUtils.invokeMethod(handler, method, builder, nodeToHandle, targetContents);
    }

    /**
     * 获得函数处理器
     * 
     * @return
     */
    protected abstract JFnHandler getFnHandler();

    @Override
    public boolean handlePageNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        MixedSqlNode mixedSqlNode = builder.parseDynamicTags(nodeToHandle);
        String name = nodeToHandle.getStringAttribute("name");
        String startRow = nodeToHandle.getStringAttribute("startRow");
        String endRow = nodeToHandle.getStringAttribute("endRow");
        String pageSize = nodeToHandle.getStringAttribute("pageSize");
        targetContents.add(handlePageNode(builder, nodeToHandle, mixedSqlNode, name, startRow, endRow, pageSize));
        return true;
    }

    /**
     * 处理分页节点
     * 
     * @param builder
     * @param nodeToHandle
     * @param mixedSqlNode
     * @param name
     * @param pageSize
     * @param endRow
     * @param startRow
     * @return
     */
    protected abstract JAbstractPageSqlNode handlePageNode(JXMLScriptBuilder builder, XNode nodeToHandle,
            MixedSqlNode mixedSqlNode, String name, String startRow, String endRow, String pageSize);

    @Override
    public boolean handleAliasNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        String body = JNodeUtils.getRequiredBody(nodeToHandle);
        String name = nodeToHandle.getStringAttribute("name", body);
        boolean as = "true".equals(nodeToHandle.getStringAttribute("as", "true"));
        StringBuilder builder2 = new StringBuilder(body);
        if (as) {
            builder2.append(" AS ");
        }
        if (useQuotAlias()) {
            builder2.append("\"");
        }
        builder2.append(name);
        if (useQuotAlias()) {
            builder2.append("\"");
        }
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * 别名添加双引号
     * 
     * @return
     */
    protected boolean useQuotAlias() {
        return false;
    }

    @Override
    public boolean handleInsertListMapperMethod(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement, JEntity entity) {
        Element updateElement = JElementBuilder.buildUpdateElement(document, method);
        String item = "item";
        Element foreachElement = JElementBuilder.buildForeachElement(document, JParamConsts.PARAM_RECORD, item);
        JProviderHelper.selectKeys(document, foreachElement, entity, item);
        JProviderHelper.bindGeneratorValues(document, foreachElement, entity, item);
        JProviderHelper.insertIntoTable(configuration, document, foreachElement, entity,
                JStringUtils.format("{}[0]", JParamConsts.PARAM_RECORD));
        JElementBuilder.appendChild(document, foreachElement,
                JStringUtils.format("({}) VALUES({});",
                        JProviderHelper.getAllColumns(entity, JEntityColumn::isInsertable),
                        JProviderHelper.getAllValues(entity, item, JEntityColumn::isInsertable)));
        updateElement.appendChild(foreachElement);
        mapperElement.appendChild(updateElement);
        return true;
    }

    /**
     * {@inheritDoc}
     * <p>
     * 默认判断方言名称{@link #name()}与数据库产品名称{@link DatabaseMetaData#getDatabaseProductName()}是否相等
     * </p>
     */
    @Override
    public boolean support(JDatabaseMetaData metaData) {
        return JStringUtils.equals(this.name(), metaData.getDatabaseProductName());
    }

    /**
     * 方言名称
     * 
     * @return
     */
    protected abstract String name();
}
