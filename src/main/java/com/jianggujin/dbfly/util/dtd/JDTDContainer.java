/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util.dtd;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.jianggujin.dbfly.util.JAssert;

public abstract class JDTDContainer extends JDTDItem {
    protected List<JDTDItem> items = new ArrayList<>();

    public JDTDContainer(JDTDItem... items) {
        super();
        this.add(items);
    }

    public JDTDContainer(String... values) {
        super();
        this.add(values);
    }

    public JDTDContainer(JDTDCardinal cardinal, JDTDItem... items) {
        super(cardinal);
        this.add(items);
    }

    public JDTDContainer(JDTDCardinal cardinal, JDTDCardinal itemCardinal, String... values) {
        super(cardinal);
        this.add(itemCardinal, values);
    }

    public void add(String... values) {
        for (String value : values) {
            JAssert.notNull(values, "values中不能包含空元素");
            this.items.add(new JDTDName(value));
        }
    }

    public void add(JDTDCardinal cardinal, String... values) {
        JAssert.notNull(cardinal, "cardinal不能为空");
        for (String value : values) {
            JAssert.notNull(values, "values中不能包含空元素");
            this.items.add(new JDTDName(value, cardinal));
        }
    }

    public void addPCData() {
        this.items.add(new JDTDPCData());
    }

    public void add(JDTDItem... items) {
        for (JDTDItem item : items) {
            JAssert.notNull(item, "items中不能包含空元素");
            this.items.add(item);
        }
    }

    public void remove(JDTDItem item) {
        JAssert.notNull(item, "item不能为空");
        this.items.remove(item);
    }

    public List<JDTDItem> getItems() {
        return this.items;
    }

    public void setItem(int index, JDTDItem item) {
        JAssert.notNull(item, "item不能为空");
        this.items.set(index, item);
    }

    public JDTDItem getItem(int index) {
        return this.items.get(index);
    }

    @Override
    public void write(PrintWriter out) throws IOException {
        JAssert.isTrue(!this.items.isEmpty(), "应至少包含一项");
        super.write(out);
    }
}
