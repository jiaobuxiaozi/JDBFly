/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.mapper;

import com.jianggujin.dbfly.mybatis.builder.mapper.delete.JDeleteByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.delete.JDeleteByIdMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.delete.JDeleteMapper;

/**
 * 基础删除
 * 
 * @author jianggujin
 *
 * @param <T>
 */
public interface JBaseDeleteMapper<T> extends JDeleteMapper<T>, JDeleteByIdMapper<T>, JDeleteByConditionMapper<T> {

}
