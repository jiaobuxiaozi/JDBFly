package com.jianggujin.dbfly.test;

import java.io.PrintWriter;

import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JDBFlyConfigDTDFactory;
import com.jianggujin.dbfly.mybatis.JDBFlyMapperDTDFactory;
import com.jianggujin.dbfly.util.dtd.JDTD;

public class DtdParserTest {
    @Test
    public void mapper() throws Exception {
        JDTD dtd = new JDBFlyMapperDTDFactory().createDTD();
        dtd.write(new PrintWriter(System.err));
    }

    @Test
    public void config() throws Exception {
        JDTD dtd = new JDBFlyConfigDTDFactory().createDTD();
        dtd.write(new PrintWriter(System.err));
    }

}
