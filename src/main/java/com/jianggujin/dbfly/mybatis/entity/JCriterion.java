/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import java.util.Collection;

/**
 * 条件规则
 * 
 * @author jianggujin
 *
 */
public class JCriterion {
    private String condition;

    private Object value;

    private Object secondValue;

    private String andOr;

    private boolean noValue;

    private boolean singleValue;

    private boolean betweenValue;

    private boolean listValue;

    private String typeHandler;

    protected JCriterion(String condition) {
        this(condition, false);
    }

    protected JCriterion(String condition, Object value, String typeHandler) {
        this(condition, value, typeHandler, false);
    }

    protected JCriterion(String condition, Object value) {
        this(condition, value, null, false);
    }

    protected JCriterion(String condition, Object value, Object secondValue, String typeHandler) {
        this(condition, value, secondValue, typeHandler, false);
    }

    protected JCriterion(String condition, Object value, Object secondValue) {
        this(condition, value, secondValue, null, false);
    }

    protected JCriterion(String condition, boolean isOr) {
        super();
        this.condition = condition;
        this.typeHandler = null;
        this.noValue = true;
        this.andOr = isOr ? "or" : "and";
    }

    protected JCriterion(String condition, Object value, String typeHandler, boolean isOr) {
        super();
        this.condition = condition;
        this.value = value;
        this.typeHandler = typeHandler;
        this.andOr = isOr ? "or" : "and";
        if (value instanceof Collection<?>) {
            this.listValue = true;
        } else {
            this.singleValue = true;
        }
    }

    protected JCriterion(String condition, Object value, boolean isOr) {
        this(condition, value, null, isOr);
    }

    protected JCriterion(String condition, Object value, Object secondValue, String typeHandler, boolean isOr) {
        super();
        this.condition = condition;
        this.value = value;
        this.secondValue = secondValue;
        this.typeHandler = typeHandler;
        this.betweenValue = true;
        this.andOr = isOr ? "or" : "and";
    }

    protected JCriterion(String condition, Object value, Object secondValue, boolean isOr) {
        this(condition, value, secondValue, null, isOr);
    }

    public String getAndOr() {
        return andOr;
    }

    public void setAndOr(String andOr) {
        this.andOr = andOr;
    }

    public String getCondition() {
        return condition;
    }

    public Object getSecondValue() {
        return secondValue;
    }

    public String getTypeHandler() {
        return typeHandler;
    }

    public Object getValue() {
        return value;
    }

    public boolean isBetweenValue() {
        return betweenValue;
    }

    public boolean isListValue() {
        return listValue;
    }

    public boolean isNoValue() {
        return noValue;
    }

    public boolean isSingleValue() {
        return singleValue;
    }
}
