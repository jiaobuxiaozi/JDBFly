package com.jianggujin.dbfly.test.entity;

import com.jianggujin.dbfly.mybatis.id.JUseGeneratedKeys;

public class TestEntityAuto {
    @JUseGeneratedKeys
    private Integer id;
    private String code;
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "TestEntityAuto [id=" + id + ", code=" + code + ", version=" + version + "]";
    }

}
