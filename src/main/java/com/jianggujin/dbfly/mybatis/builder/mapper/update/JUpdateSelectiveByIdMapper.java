/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.mapper.update;

import com.jianggujin.dbfly.mybatis.builder.JMappedStatementProvider;
import com.jianggujin.dbfly.mybatis.builder.provider.JBaseUpdateProvider;

/**
 * 根据主键更新属性不为null的值
 * 
 * @author jianggujin
 *
 * @param <T>
 */
public interface JUpdateSelectiveByIdMapper<T> {

    /**
     * 根据主键更新属性不为null的值
     *
     * @param id
     * @return
     */
    @JMappedStatementProvider(type = JBaseUpdateProvider.class)
    int updateSelectiveById(T id);
}
