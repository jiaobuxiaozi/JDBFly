/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.page;

import org.apache.ibatis.scripting.xmltags.DynamicContext;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;

import com.jianggujin.dbfly.util.JStringUtils;

/**
 * 分页节点Hsqldb实现
 * 
 * @author jianggujin
 *
 */
public class JHsqldbPageSqlNode extends JAbstractPageSqlNode {

    private final TextSqlNode limitSqlNode;
    private final TextSqlNode offsetSqlNode;

    public JHsqldbPageSqlNode(MixedSqlNode mixedSqlNode, String name, String startRow, String endRow, String pageSize) {
        super(mixedSqlNode, name, startRow, endRow, pageSize);
        // 指定参数名称
        String limitSql = null, offsetSql = null;
        if (!dynamicParse) {
            limitSql = JStringUtils.format("limit #{{}.{}}", this.name, this.pageSize);
            limitSql = JStringUtils.format("OFFSET #{{}.{}}", this.name, this.startRow);
        } else {
            limitSql = JStringUtils.format(" limit #{{}}", DYNAMIC_PAGE_SIZE_KEY);
            limitSql = JStringUtils.format(" OFFSET #{{}}", DYNAMIC_START_ROW_KEY);
        }
        this.limitSqlNode = new TextSqlNode(limitSql);
        this.offsetSqlNode = new TextSqlNode(offsetSql);
    }

    @Override
    protected void applyNode(DynamicContext context) {
        super.applyNode(context);
        limitSqlNode.apply(context);
        offsetSqlNode.apply(context);
    }
}