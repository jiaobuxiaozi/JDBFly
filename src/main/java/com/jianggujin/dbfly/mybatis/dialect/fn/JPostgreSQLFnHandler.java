/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.fn;

import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;

import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.dialect.translate.JFormatTranslate;
import com.jianggujin.dbfly.mybatis.dialect.translate.JPostgreSQLDateFormatTranslate;
import com.jianggujin.dbfly.mybatis.util.JNodeUtils;

/**
 * 扩展的函数处理器PostgreSQL实现
 * 
 * @author jianggujin
 *
 */
public class JPostgreSQLFnHandler extends JDefaultFnHandler {

    private JFormatTranslate dateFormatTranslate = new JPostgreSQLDateFormatTranslate();

    /**
     * 处理position，返回指定的子字串的位置，使用POSITION
     */
    @Override
    public boolean handleFnPositionNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" POSITION(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "sub")).append(" in ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * 处理sub_str，从字符串str的start位置截取长度为length的子字符串，默认使用SUBSTR
     */
    @Override
    public boolean handleFnSubStrNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" SUBSTR(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(", ").append(JNodeUtils.getRequiredValue(nodeToHandle, "start")).append(", ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "length")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnDateFormatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" TO_CHAR(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "date")).append(", '");
        dateFormatTranslate.translate(builder2, nodeToHandle.getStringAttribute("format"));
        builder2.append("') ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnStrToDateNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" TO_DATE(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(", '");
        dateFormatTranslate.translate(builder2, nodeToHandle.getStringAttribute("format"));
        builder2.append("') ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnNvlNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" COALESCE(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "exp1")).append(", ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "exp2")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }
}
