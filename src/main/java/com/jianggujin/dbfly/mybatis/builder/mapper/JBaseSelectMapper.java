/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.mapper;

import com.jianggujin.dbfly.mybatis.builder.mapper.select.JExistsByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JExistsByIdMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JExistsMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectAllMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectByIdMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectCountAllMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectCountByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectCountMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectOneByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectOneMapper;

/**
 * 基础查询
 * 
 * @author jianggujin
 *
 * @param <T>
 */
public interface JBaseSelectMapper<T> extends JSelectOneMapper<T>, JSelectMapper<T>, JSelectAllMapper<T>,
        JSelectCountMapper<T>, JSelectCountAllMapper<T>, JSelectByIdMapper<T>, JExistsMapper<T>, JExistsByIdMapper<T>,
        JExistsByConditionMapper<T>, JSelectByConditionMapper<T>, JSelectCountByConditionMapper<T>,
        JSelectOneByConditionMapper<T> {

}
