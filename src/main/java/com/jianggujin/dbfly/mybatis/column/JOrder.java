/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.column;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.jianggujin.dbfly.mybatis.constant.JOrderStrategy;

/**
 * 排序，用于指定实体类的属性是否为排序属性
 * 
 * @author jianggujin
 *
 */
@Target(FIELD)
@Retention(RUNTIME)
public @interface JOrder {
    /**
     * 排序策略
     * 
     * @return
     */
    JOrderStrategy value() default JOrderStrategy.ASC;

    /**
     * 优先级, 值小的优先
     * 
     * @return
     */
    int priority() default 1;
}
