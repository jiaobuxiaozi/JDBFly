/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.constant;

/**
 * 主键生成策略
 * 
 * @author jianggujin
 *
 */
public enum JIdGeneratorStrategy {
    /**
     * 不作任何处理
     */
    NONE,
    /**
     * 使用JDBC自动生成
     */
    JDBC,
    /**
     * 通过执行SQL语句查询
     */
    IDENTITY,
    /**
     * 自定义JAVA代码生成
     */
    JAVA
}
