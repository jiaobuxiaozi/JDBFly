/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.fn;

import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;

import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.util.JNodeUtils;
import com.jianggujin.dbfly.util.JDBFlyException;
import com.jianggujin.dbfly.util.JStringUtils;

/**
 * 扩展的函数处理器默认实现
 * 
 * @author jianggujin
 *
 */
public class JDefaultFnHandler implements JFnHandler {

    /**
     * {@inheritDoc}，默认使用ASCII
     */
    @Override
    public boolean handleFnAsciiNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" ASCII(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用||拼接
     */
    @Override
    public boolean handleFnConcatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder();
        boolean first = true;
        int count = 0;
        for (XNode node : nodeToHandle.getChildren()) {
            String value = node.getStringBody();
            if (JStringUtils.isEmpty(value)) {
                break;
            }
            if (!first) {
                builder2.append(" || ");
            }
            first = false;
            builder2.append(value);
            count++;
        }
        if (count < 21) {
            throw new JDBFlyException("{}节点不合法，需要至少包含两条数据", nodeToHandle.getName());
        }
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用CHAR_LENGTH
     */
    @Override
    public boolean handleFnCharLengthNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" CHAR_LENGTH(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用BIT_LENGTH
     */
    @Override
    public boolean handleFnBitLengthNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" BIT_LENGTH(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用POSITION
     */
    @Override
    public boolean handleFnPositionNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" POSITION(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "sub")).append(", ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用REPLACE
     */
    @Override
    public boolean handleFnReplaceNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" REPLACE(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(", ").append(JNodeUtils.getRequiredValue(nodeToHandle, "from")).append(", ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "to")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用SUBSTRING
     */
    @Override
    public boolean handleFnSubStrNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" SUBSTRING(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(", ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "start")).append(", ")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "length")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用UPPER
     */
    @Override
    public boolean handleFnUpperNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" UPPER(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用LOWER
     */
    @Override
    public boolean handleFnLowerNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" LOWER(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用TRIM
     */
    @Override
    public boolean handleFnTrimNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" TRIM(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用:TRIM
     */
    @Override
    public boolean handleFnLTrimNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" LTRIM(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * {@inheritDoc}，默认使用RTRIM
     */
    @Override
    public boolean handleFnRTrimNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" RTRIM(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnDateFormatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        return false;
    }

    @Override
    public boolean handleFnStrToDateNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        return false;
    }

    @Override
    public boolean handleFnNvlNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        return false;
    }

}
