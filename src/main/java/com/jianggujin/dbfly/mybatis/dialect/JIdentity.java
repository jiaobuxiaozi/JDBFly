/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect;

import com.jianggujin.dbfly.mybatis.constant.JKeyOrder;

/**
 * 主键语句与生成时期
 * 
 * @author jianggujin
 *
 */
public class JIdentity {
    /**
     * 获取主键的SQL
     */
    private final String retrievalStatement;
    /**
     * 主键生成时期
     */
    private final JKeyOrder keyOrder;

    public JIdentity(String retrievalStatement, JKeyOrder keyOrder) {
        this.retrievalStatement = retrievalStatement;
        this.keyOrder = keyOrder;
    }

    public JIdentity(String retrievalStatement) {
        this(retrievalStatement, JKeyOrder.BEFORE);
    }

    /**
     * 获取主键的SQL
     * 
     * @return
     */
    public String getRetrievalStatement() {
        return retrievalStatement;
    }

    /**
     * 主键生成时期
     * 
     * @return
     */
    public JKeyOrder getKeyOrder() {
        return keyOrder;
    }
}
