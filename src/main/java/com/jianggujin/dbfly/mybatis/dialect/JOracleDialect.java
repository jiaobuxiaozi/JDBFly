/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect;

import java.lang.reflect.Method;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.MixedSqlNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.builder.JParamConsts;
import com.jianggujin.dbfly.mybatis.dialect.fn.JFnHandler;
import com.jianggujin.dbfly.mybatis.dialect.fn.JOracleFnHandler;
import com.jianggujin.dbfly.mybatis.dialect.page.JAbstractPageSqlNode;
import com.jianggujin.dbfly.mybatis.dialect.page.JOraclePageSqlNode;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.entity.JEntityColumn;
import com.jianggujin.dbfly.mybatis.util.JElementBuilder;
import com.jianggujin.dbfly.mybatis.util.JProviderHelper;
import com.jianggujin.dbfly.util.JStringUtils;

/**
 * 用于Oracle的方言实现
 * 
 * @author jianggujin
 *
 */
public class JOracleDialect extends JAbstarctDialect {
    private JOracleFnHandler fnHandler = new JOracleFnHandler();

    @Override
    protected JFnHandler getFnHandler() {
        return fnHandler;
    }

    @Override
    protected JAbstractPageSqlNode handlePageNode(JXMLScriptBuilder builder, XNode nodeToHandle,
            MixedSqlNode mixedSqlNode, String name, String startRow, String endRow, String pageSize) {
        return new JOraclePageSqlNode(mixedSqlNode, name, startRow, endRow, pageSize);
    }

    @Override
    protected boolean useQuotAlias() {
        return true;
    }

    @Override
    public boolean handleInsertListMapperMethod(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement, JEntity entity) {
        Element insertElement = JElementBuilder.buildInsertElement(document, method);
        String item = "item";
        JElementBuilder.appendChild(document, insertElement, "INSERT ALL");
        Element foreachElement = JElementBuilder.buildForeachElement(document, JParamConsts.PARAM_RECORD, item);
        JProviderHelper.selectKeys(document, foreachElement, entity, item);
        JProviderHelper.bindGeneratorValues(document, foreachElement, entity, item);
        JElementBuilder.appendChild(document, foreachElement, " INTO");
        JProviderHelper.dynamicTable(configuration, document, foreachElement, entity,
                JStringUtils.format("{}[0]", JParamConsts.PARAM_RECORD));
        JElementBuilder.appendChild(document, foreachElement,
                JStringUtils.format("({}) VALUES({})",
                        JProviderHelper.getAllColumns(entity, JEntityColumn::isInsertable),
                        JProviderHelper.getAllValues(entity, item, JEntityColumn::isInsertable)));
        insertElement.appendChild(foreachElement);
        mapperElement.appendChild(insertElement);
        return true;
    }

    @Override
    public String name() {
        return "Oracle";
    }
}
