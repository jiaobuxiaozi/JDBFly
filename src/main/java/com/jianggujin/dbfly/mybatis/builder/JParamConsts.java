/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder;

/**
 * 参数常量
 * 
 * @author jianggujin
 *
 */
public interface JParamConsts {
    /**
     * 实体对象参数
     */
    String PARAM_RECORD = "record";
    /**
     * 动态条件参数
     */
    String PARAM_CONDITION = "condition";
    /**
     * 分页参数
     */
    String PARAM_PAGE = "page";
    /**
     * Mybatis默认参数名称
     */
    String MYBATIS_PARAMETER = "_parameter";
    /**
     * Mybatis默认参数名称前缀
     */
    String MYBATIS_PARAMETER_FREFIX = MYBATIS_PARAMETER + ".";
    /**
     * Mybatis数据库标识
     */
    String MYBATIS_DATABASE_ID = "_databaseId";
}
