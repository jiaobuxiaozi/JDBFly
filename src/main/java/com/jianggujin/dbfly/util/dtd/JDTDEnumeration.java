/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util.dtd;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.jianggujin.dbfly.util.JAssert;

public class JDTDEnumeration implements JDTDOutput {
    protected List<String> items = new ArrayList<>();

    public JDTDEnumeration(String... items) {
        this.add(items);
    }

    public void add(String... items) {
        for (String item : items) {
            JAssert.notNull(item, "items中不能包含空元素");
            this.items.add(item);
        }
    }

    public void remove(String item) {
        JAssert.notNull(item, "item不能为空");
        this.items.remove(item);
    }

    public List<String> getItems() {
        return this.items;
    }

    public void clear() {
        this.items.clear();
    }

    @Override
    public void write(PrintWriter out) throws IOException {
        JAssert.isTrue(!this.items.isEmpty(), "应至少包含一项");
        out.print("(");
        boolean isFirst = true;
        for (String item : items) {
            if (!isFirst)
                out.print(" | ");
            isFirst = false;
            out.print(item);
        }
        out.print(")");
    }

    public void setItem(int index, String item) {
        JAssert.notNull(item, "item不能为空");
        this.items.set(index, item);
    }

    public String getItem(int index) {
        return this.items.get(index);
    }
}
