package com.jianggujin.dbfly.test.entity;

import com.jianggujin.dbfly.mybatis.id.JUseIdGenerator;
import com.jianggujin.dbfly.mybatis.table.JTable;
import com.jianggujin.dbfly.test.IdGenerator;

@JTable("test_entity")
public class UseIdGeneratorEntity {
    @JUseIdGenerator(IdGenerator.class)
    private Integer id;
    private String code;
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "TestEntityAuto [id=" + id + ", code=" + code + ", version=" + version + "]";
    }

}
