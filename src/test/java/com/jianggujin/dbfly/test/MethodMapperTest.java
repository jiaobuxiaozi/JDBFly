package com.jianggujin.dbfly.test;

import java.io.InputStream;
import java.util.Arrays;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JSqlSessionFactoryBuilder;
import com.jianggujin.dbfly.test.mapper.TestMethodMapper;

public class MethodMapperTest {
    private SqlSession session = null;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
        session = sqlSessionFactory.openSession();
    }

    @Test
    public void findByIdNotNull() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdNotNull());
    }

    @Test
    public void findByIdNull() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdNull());
    }

    @Test
    public void findByIdNot() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdNot(1));
    }

    @Test
    public void findById() {
        System.err.println(session.getMapper(TestMethodMapper.class).findById(1));
    }

    @Test
    public void findByIdGreaterThan() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdGreaterThan(1));
    }

    @Test
    public void findByIdGreaterThanEqual() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdGreaterThanEqual(1));
    }

    @Test
    public void findByIdLessThan() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdLessThan(1));
    }

    @Test
    public void findByIdLessThanEqual() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdLessThanEqual(1));
    }

    @Test
    public void findByIdBefore() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdBefore(1));
    }

    @Test
    public void findByIdBeforeEqual() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdBeforeEqual(1));
    }

    @Test
    public void findByIdAfter() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdAfter(1));
    }

    @Test
    public void findByIdAfterEqual() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdAfterEqual(1));
    }

    @Test
    public void findByIdNotIn() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdNotIn(Arrays.asList(1, 2)));
    }

    @Test
    public void findByIdIn() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdIn(Arrays.asList(1, 2)));
    }

    @Test
    public void findByIdBetween() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdBetween(1, 3));
    }

    @Test
    public void findByIdNotBetween() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByIdNotBetween(1, 3));
    }

    @Test
    public void findByCodeLike() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByCodeLike("2"));
    }

    @Test
    public void findByCodeNotLike() {
        System.err.println(session.getMapper(TestMethodMapper.class).findByCodeNotLike("2"));
    }

    @Test
    public void deleteById() {
        System.err.println(session.getMapper(TestMethodMapper.class).deleteById(3));
    }

    @Test
    public void findAll() {
        System.err.println(session.getMapper(TestMethodMapper.class).findAll());
    }

    @Test
    public void selectCodeById() {
        System.err.println(session.getMapper(TestMethodMapper.class).selectCodeById(1));
    }

    @Test
    public void selectCodeBy() {
        System.err.println(session.getMapper(TestMethodMapper.class).selectCodeBy());
    }

    @After
    public void destory() {
        if (session != null) {
            session.close();
        }
    }
}
