/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.additional;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jianggujin.dbfly.mybatis.builder.JMappedStatementProvider;

/**
 * 根据主键集合查询
 * 
 * @author jianggujin
 *
 * @param <T>
 * @param <PK>
 */
public interface JSelectByIdsMapper<T, PK> {
    /**
     * 根据主键字符串进行查询，类中只有存在一个主键字段
     *
     * @param ids
     * @return
     */
    @JMappedStatementProvider(type = JAdditionalProvider.class)
    List<T> selectByIds(@Param("ids") List<PK> ids);
}
