/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.mapper.select;

import java.util.List;

import com.jianggujin.dbfly.mybatis.builder.JMappedStatementProvider;
import com.jianggujin.dbfly.mybatis.builder.provider.JBaseSelectProvider;
import com.jianggujin.dbfly.mybatis.entity.JCondition;

/**
 * 根据Condition条件进行查询
 * 
 * @author jianggujin
 *
 * @param <T>
 */
public interface JSelectByConditionMapper<T> {
    /**
     * 根据Condition条件进行查询
     *
     * @param condition
     * @return
     */
    @JMappedStatementProvider(type = JBaseSelectProvider.class)
    List<T> selectByCondition(JCondition<T> condition);
}
