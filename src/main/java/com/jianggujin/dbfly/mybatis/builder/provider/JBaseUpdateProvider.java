/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.provider;

import java.lang.reflect.Method;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.builder.JParamConsts;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.util.JElementBuilder;
import com.jianggujin.dbfly.mybatis.util.JProviderHelper;

public class JBaseUpdateProvider {
    /**
     * 通过主键更新全部字段
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void updateById(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element updateElement = JElementBuilder.buildUpdateElement(document, method);
        JProviderHelper.bindGeneratorValue(document, updateElement, entity, false, null);
        JProviderHelper.updateTable(configuration, document, updateElement, entity, null);
        JProviderHelper.updateSetColumns(document, updateElement, entity, false, null);
        JProviderHelper.wherePKColumns(document, updateElement, entity, null);
        mapperElement.appendChild(updateElement);
    }

    /**
     * 通过主键更新不为null的字段
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void updateSelectiveById(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element updateElement = JElementBuilder.buildUpdateElement(document, method);
        JProviderHelper.bindGeneratorValue(document, updateElement, entity, false, null);
        JProviderHelper.updateTable(configuration, document, updateElement, entity, null);
        JProviderHelper.updateSetColumns(document, updateElement, entity, true, null);
        JProviderHelper.wherePKColumns(document, updateElement, entity, null);
        mapperElement.appendChild(updateElement);
    }

    /**
     * 根据Condition更新非null字段
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void updateSelectiveByCondition(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element updateElement = JElementBuilder.buildUpdateElement(document, method);
        JProviderHelper.bindGeneratorValue(document, updateElement, entity, false, JParamConsts.PARAM_RECORD);
        JProviderHelper.updateTable(configuration, document, updateElement, entity, JParamConsts.PARAM_CONDITION);
        JProviderHelper.updateSetColumns(document, updateElement, entity, true, JParamConsts.PARAM_RECORD);
        JProviderHelper.conditionWhereClause(document, updateElement, JParamConsts.PARAM_CONDITION);
        mapperElement.appendChild(updateElement);
    }

    /**
     * 根据Condition更新
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     */
    public void updateByCondition(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element updateElement = JElementBuilder.buildUpdateElement(document, method);
        JProviderHelper.bindGeneratorValue(document, updateElement, entity, false, JParamConsts.PARAM_RECORD);
        JProviderHelper.updateTable(configuration, document, updateElement, entity, JParamConsts.PARAM_CONDITION);
        JProviderHelper.updateSetColumns(document, updateElement, entity, false, JParamConsts.PARAM_RECORD);
        JProviderHelper.conditionWhereClause(document, updateElement, JParamConsts.PARAM_CONDITION);
        mapperElement.appendChild(updateElement);
    }

}
