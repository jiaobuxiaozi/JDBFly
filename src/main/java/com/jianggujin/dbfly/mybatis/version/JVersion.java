/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.version;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jianggujin.dbfly.mybatis.value.JGeneratedValue;

/**
 * 版本号，用于乐观锁，一个实体类中只能存在一个，不能与主键策略一起使用，优先级高于{@link JGeneratedValue}
 * 
 * @author jianggujin
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JVersion {
    /**
     * 版本号生成，默认算法支持 Integer 和 Long，在原基础上 +1
     * 
     * @return
     */
    Class<? extends JVersionGenerator> value() default JDefaultVersionGenerator.class;
}
