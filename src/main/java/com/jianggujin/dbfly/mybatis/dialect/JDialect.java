/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect;

import java.lang.reflect.Method;
import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.util.JDatabaseMetaData;

/**
 * 方言
 * 
 * @author jianggujin
 *
 */
public interface JDialect {

    /**
     * 查询或生成主键的SQL以及生成顺序，如不支持则返回null
     * 
     * @return
     */
    JIdentity getIdentity();

    /**
     * 处理函数节点， 返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     * @return
     */
    boolean handleFnNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理分页节点， 返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     * @return
     */
    boolean handlePageNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理别名节点， 返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     * @return
     */
    boolean handleAliasNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理批量插入，返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     * @param entity
     * @return
     */
    boolean handleInsertListMapperMethod(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement, JEntity entity);

    /**
     * 判断当前方言是否支持指定数据库元数据
     * 
     * @param metaData
     * @return
     */
    boolean support(JDatabaseMetaData metaData);

}
