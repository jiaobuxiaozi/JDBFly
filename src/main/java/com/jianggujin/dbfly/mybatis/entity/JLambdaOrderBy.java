/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import com.jianggujin.dbfly.mybatis.util.JLambdaPropertyUtils;
import com.jianggujin.dbfly.mybatis.util.JPropertyFunction;

/**
 * @author jianggujin
 *
 */
public class JLambdaOrderBy<T> {
    private final JOrderBy orderBy;

    protected JLambdaOrderBy(JOrderBy orderBy) {
        this.orderBy = orderBy;
    }

    public <R> JLambdaOrderBy<T> orderBy(JPropertyFunction<T, R> function) {
        this.orderBy.orderBy(JLambdaPropertyUtils.resolve(function));
        return this;
    }

    public JLambdaOrderBy<T> desc() {
        this.orderBy.desc();
        return this;
    }

    public JLambdaOrderBy<T> asc() {
        this.orderBy.asc();
        return this;
    }

    public <R> JLambdaOrderBy<T> orderByAsc(JPropertyFunction<T, R> function) {
        return this.orderBy(function).asc();
    }

    public <R> JLambdaOrderBy<T> orderByDesc(JPropertyFunction<T, R> function) {
        return this.orderBy(function).desc();
    }

    public JOrderBy getOrderBy() {
        return orderBy;
    }
}
