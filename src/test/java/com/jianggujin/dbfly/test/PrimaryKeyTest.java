package com.jianggujin.dbfly.test;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.Before;
import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JSqlSessionFactoryBuilder;
import com.jianggujin.dbfly.test.entity.TestEntityAuto;
import com.jianggujin.dbfly.test.entity.UseIdGeneratorEntity;
import com.jianggujin.dbfly.test.entity.UseIdentityDialectEntity;
import com.jianggujin.dbfly.test.mapper.IdentityDialectMapper;
import com.jianggujin.dbfly.test.mapper.TestEntityAutoMapper;
import com.jianggujin.dbfly.test.mapper.UseIdGeneratorMapper;

public class PrimaryKeyTest {
    private SqlSession session = null;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
        session = sqlSessionFactory.openSession();
    }

    @Test
    public void generatedKeys() {
        TestEntityAuto record = new TestEntityAuto();
        record.setCode("100");
        record.setVersion(1);
        System.err.println(session.getMapper(TestEntityAutoMapper.class).insert(record));
        System.err.println(record);
    }

    @Test
    public void identityDialect() {
        UseIdentityDialectEntity record = new UseIdentityDialectEntity();
        record.setCode("100");
        record.setVersion(1);
        System.err.println(session.getMapper(IdentityDialectMapper.class).insert(record));
        System.err.println(record);
    }

    @Test
    public void idGenerator() {
        UseIdGeneratorEntity record = new UseIdGeneratorEntity();
        record.setCode("100");
        record.setVersion(1);
        System.err.println(session.getMapper(UseIdGeneratorMapper.class).insert(record));
        System.err.println(record);
    }
}
