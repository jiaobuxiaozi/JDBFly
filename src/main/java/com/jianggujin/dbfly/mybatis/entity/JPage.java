/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import java.io.Serializable;

/**
 * 分页信息
 * 
 * @author jianggujin
 *
 */
public class JPage implements Serializable {

    /**
     * 页码，从1开始
     */
    private int pageNum;
    /**
     * 页面大小
     */
    private int pageSize;
    /**
     * 起始行
     */
    private int startRow;
    /**
     * 末行
     */
    private int endRow;
    /**
     * 总数
     */
    private long total;
    /**
     * 总页数
     */
    private int pages;

    public JPage(JPage page) {
        this.pageNum = page.pageNum;
        this.pageSize = page.pageSize;
        this.startRow = page.startRow;
        this.endRow = page.endRow;
        this.total = page.total;
        this.pages = page.pages;
    }

    public JPage(int pageNum, int pageSize) {
        if (pageNum == 1 && pageSize == Integer.MAX_VALUE) {
            pageSize = 0;
        }
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        // 分页合理化，针对不合理的页码自动处理
        if (this.pageNum <= 0) {
            this.pageNum = 1;
        }
        calculateStartAndEndRow();
    }

    public JPage(int pageNum, int pageSize, long total) {
        if (pageNum == 1 && pageSize == Integer.MAX_VALUE) {
            pageSize = 0;
        }
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        // 分页合理化，针对不合理的页码自动处理
        if (this.pageNum <= 0) {
            this.pageNum = 1;
        }
        this.total = total;
        if (total == -1) {
            pages = 1;
            return;
        }
        if (pageSize > 0) {
            pages = (int) ((total + pageSize - 1) / pageSize);
            // pages = (int) (total / pageSize + ((total % pageSize == 0) ? 0 : 1));
        } else {
            pages = 0;
        }
        // 分页合理化，针对不合理的页码自动处理
        if (pageNum > pages) {
            if (pages != 0) {
                pageNum = pages;
            }
        }
        calculateStartAndEndRow();
    }

    /**
     * 计算起止行号
     */
    private void calculateStartAndEndRow() {
        this.startRow = this.pageNum > 0 ? (this.pageNum - 1) * this.pageSize : 0;
        this.endRow = this.startRow + this.pageSize * (this.pageNum > 0 ? 1 : 0);
    }

    public int getPageNum() {
        return pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getStartRow() {
        return startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public long getTotal() {
        return total;
    }

    public int getPages() {
        return pages;
    }

    @Override
    public String toString() {
        return "[pageNum=" + pageNum + ", pageSize=" + pageSize + ", startRow=" + startRow + ", endRow=" + endRow
                + ", total=" + total + ", pages=" + pages + "]";
    }

}
