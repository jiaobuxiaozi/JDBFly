/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.provider;

import java.lang.reflect.Method;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.util.JElementBuilder;
import com.jianggujin.dbfly.mybatis.util.JProviderHelper;

public class JBaseInsertProvider {

    public void insert(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        insert(configuration, mapperClass, method, document, mapperElement, false);
    }

    public void insertSelective(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        insert(configuration, mapperClass, method, document, mapperElement, true);
    }

    private void insert(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement, boolean notNull) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        Element insertElement = JElementBuilder.buildInsertElement(document, method);
        JProviderHelper.selectKey(document, insertElement, entity, null);
        JProviderHelper.bindGeneratorValue(document, insertElement, entity, true, null);
        JProviderHelper.insertIntoTable(configuration, document, insertElement, entity, null);
        JProviderHelper.insertColumns(document, insertElement, entity, notNull, null);
        JProviderHelper.insertValues(document, insertElement, entity, notNull, null);
        mapperElement.appendChild(insertElement);
    }
}
