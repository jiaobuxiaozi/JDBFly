/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.fn;

import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;

import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.dialect.translate.JFormatTranslate;
import com.jianggujin.dbfly.mybatis.dialect.translate.JMySQLDateFormatTranslate;
import com.jianggujin.dbfly.mybatis.util.JNodeUtils;
import com.jianggujin.dbfly.util.JDBFlyException;
import com.jianggujin.dbfly.util.JStringUtils;

/**
 * 扩展的函数处理器MySQL实现
 * 
 * @author jianggujin
 *
 */
public class JMySQLFnHandler extends JDefaultFnHandler {

    private JFormatTranslate dateFormatTranslate = new JMySQLDateFormatTranslate();

    /**
     * 处理concat，字符串拼接，使用CONCAT函数
     */
    @Override
    public boolean handleFnConcatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder();
        boolean first = true;
        int count = 0;
        for (XNode node : nodeToHandle.getChildren()) {
            String value = node.getStringBody();
            if (JStringUtils.isEmpty(value)) {
                break;
            }
            if (!first) {
                builder2.append(", ");
            }
            first = false;
            builder2.append(value);
            count++;
        }
        if (count > 1) {
            builder2.insert(0, " CONCAT(");
            builder2.append(") ");
        } else if (count < 1) {
            throw new JDBFlyException("{}节点不合法，需要至少包含一条数据", nodeToHandle.getName());
        }
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * 处理bit_length，返回字符串的字节数，使用LENGTH
     */
    @Override
    public boolean handleFnBitLengthNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" LENGTH(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    /**
     * 处理position，返回指定的子字串的位置，使用LOCATE
     */
    @Override
    public boolean handleFnPositionNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" LOCATE(").append(JNodeUtils.getRequiredValue(nodeToHandle, "sub"))
                .append(", ").append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnDateFormatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" DATE_FORMAT(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "date")).append(", '");
        dateFormatTranslate.translate(builder2, nodeToHandle.getStringAttribute("format"));
        builder2.append("') ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnStrToDateNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" STR_TO_DATE(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(", '");
        dateFormatTranslate.translate(builder2, nodeToHandle.getStringAttribute("format"));
        builder2.append("') ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnNvlNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" IFNULL(").append(JNodeUtils.getRequiredValue(nodeToHandle, "exp1"))
                .append(", ").append(JNodeUtils.getRequiredValue(nodeToHandle, "exp2")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

}
