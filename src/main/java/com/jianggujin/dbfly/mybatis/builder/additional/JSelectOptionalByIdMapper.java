/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.additional;

import java.util.Optional;

import com.jianggujin.dbfly.mybatis.builder.JMappedStatementProvider;
import com.jianggujin.dbfly.mybatis.builder.provider.JBaseSelectProvider;

/**
 * 根据主键字段进行查询，方法参数必须包含完整的主键属性，查询条件使用等号，需要依赖mybatis3.5+
 * 
 * @author jianggujin
 *
 * @param <T>
 */
public interface JSelectOptionalByIdMapper<T> {

    /**
     * 根据主键字段进行查询，方法参数必须包含完整的主键属性，查询条件使用等号
     *
     * @param id
     * @return
     */
    @JMappedStatementProvider(type = JBaseSelectProvider.class)
    Optional<T> selectById(Object id);
}
