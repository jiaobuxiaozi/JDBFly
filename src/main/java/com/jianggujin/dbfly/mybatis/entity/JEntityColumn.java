/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import java.lang.reflect.Field;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import com.jianggujin.dbfly.mybatis.value.JValueGenerator;
import com.jianggujin.dbfly.mybatis.version.JVersionGenerator;

/**
 * 实体列
 * 
 * @author jianggujin
 *
 */
public class JEntityColumn implements Comparable<JEntityColumn> {
    /**
     * 实体信息
     */
    private final JEntity entity;
    /**
     * 属性名称
     */
    private final String property;
    /**
     * 列名称
     */
    private String column;
    /**
     * java类型
     */
    private final Class<?> javaType;
    /**
     * jdbc类型
     */
    private JdbcType jdbcType;
    /**
     * 类型处理器
     */
    private Class<? extends TypeHandler<?>> typeHandler;
    /**
     * 是否排序属性
     */
    private boolean order = false;
    /**
     * 是否可查询
     */
    private boolean selectable = true;
    /**
     * 是否可插入
     */
    private boolean insertable = true;
    /**
     * 是否可修改
     */
    private boolean updatable = true;
    /**
     * 是否增加空字符串判断，仅当Java类型为字符串时生效
     */
    private boolean notEmpty = false;
    /**
     * 是否设置 javaType
     */
    private boolean useJavaType = false;
    /**
     * 对应的字段信息
     *
     */
    private final Field field;
    /**
     * Value生成器实现类
     */
    private Class<? extends JValueGenerator> valueGeneratorClass;
    /**
     * Version生成器实现类
     */
    private Class<? extends JVersionGenerator> versionGeneratorClass;

    protected JEntityColumn(JEntity entity, Field field) {
        this.entity = entity;
        this.field = field;
        this.property = field.getName();
        this.javaType = field.getType();
    }

    /**
     * 设置列名称
     * 
     * @param column
     */
    protected void setColumn(String column) {
        this.column = column;
    }

    /**
     * 设置Jdbc类型
     * 
     * @param jdbcType
     */
    protected void setJdbcType(JdbcType jdbcType) {
        this.jdbcType = jdbcType;
    }

    /**
     * 设置类型处理器
     * 
     * @param typeHandler
     */
    protected void setTypeHandler(Class<? extends TypeHandler<?>> typeHandler) {
        this.typeHandler = typeHandler;
    }

    /**
     * 设置是否排序列
     * 
     * @param order
     */
    protected void setOrder(boolean order) {
        this.order = order;
    }

    /**
     * 设置是否允许查询
     * 
     * @param selectable
     */
    protected void setSelectable(boolean selectable) {
        this.selectable = selectable;
    }

    /**
     * 设置是否允许插入
     * 
     * @param insertable
     */
    protected void setInsertable(boolean insertable) {
        this.insertable = insertable;
    }

    /**
     * 设置是否允许修改
     * 
     * @param updatable
     */
    protected void setUpdatable(boolean updatable) {
        this.updatable = updatable;
    }

    /**
     * 获得实体信息
     * 
     * @return
     */
    public JEntity getEntity() {
        return entity;
    }

    /**
     * 获得属性名称
     * 
     * @return
     */
    public String getProperty() {
        return property;
    }

    /**
     * 获得列名称
     * 
     * @return
     */
    public String getColumn() {
        return column;
    }

    /**
     * 获得Java类型
     * 
     * @return
     */
    public Class<?> getJavaType() {
        return javaType;
    }

    /**
     * 获得Jdbc类型
     * 
     * @return
     */
    public JdbcType getJdbcType() {
        return jdbcType;
    }

    /**
     * 获得类型处理器
     * 
     * @return
     */
    public Class<? extends TypeHandler<?>> getTypeHandler() {
        return typeHandler;
    }

    /**
     * 是否为主键列
     * 
     * @return
     */
    public boolean isId() {
        return this instanceof JEntityIdColumn;
    }

    /**
     * 是否为排序列
     * 
     * @return
     */
    public boolean isOrder() {
        return order;
    }

    /**
     * 是否允许查询
     * 
     * @return
     */
    public boolean isSelectable() {
        return selectable;
    }

    /**
     * 是否允许插入
     * 
     * @return
     */
    public boolean isInsertable() {
        return insertable;
    }

    /**
     * 是否允许修改
     * 
     * @return
     */
    public boolean isUpdatable() {
        return updatable;
    }

    /**
     * 获得关联字段
     * 
     * @return
     */
    public Field getField() {
        return field;
    }

    /**
     * 是否增加空字符串判断，仅当Java类型为字符串时生效
     * 
     * @return
     */
    public boolean isNotEmpty() {
        return notEmpty;
    }

    /**
     * 是否增加空字符串判断，仅当Java类型为字符串时生效
     * 
     * @param notEmpty
     */
    protected void setNotEmpty(boolean notEmpty) {
        this.notEmpty = notEmpty;
    }

    /**
     * 是否设置 javaType
     * 
     * @return
     */
    public boolean isUseJavaType() {
        return useJavaType;
    }

    /**
     * 是否设置 javaType
     * 
     * @param useJavaType
     */
    protected void setUseJavaType(boolean useJavaType) {
        this.useJavaType = useJavaType;
    }

    /**
     * Value生成器实现类
     * 
     * @param valueGeneratorClass
     */
    protected void setValueGeneratorClass(Class<? extends JValueGenerator> valueGeneratorClass) {
        this.valueGeneratorClass = valueGeneratorClass;
    }

    /**
     * Value生成器实现类
     * 
     * @return
     */
    public Class<? extends JValueGenerator> getValueGeneratorClass() {
        return valueGeneratorClass;
    }

    /**
     * 是否使用Value自动生成
     * 
     * @return
     */
    public boolean isValueGenerator() {
        return this.valueGeneratorClass != null;
    }

    /**
     * Version生成器实现类
     * 
     * @param versionGeneratorClass
     */
    protected void setVersionGeneratorClass(Class<? extends JVersionGenerator> versionGeneratorClass) {
        this.versionGeneratorClass = versionGeneratorClass;
    }

    /**
     * Version生成器实现类
     * 
     * @return
     */
    public Class<? extends JVersionGenerator> getVersionGeneratorClass() {
        return versionGeneratorClass;
    }

    /**
     * 是否使用Version
     * 
     * @return
     */
    public boolean isVersion() {
        return this.versionGeneratorClass != null;
    }

    @Override
    public int compareTo(JEntityColumn o) {
        if (this.isId() && o.isId()) {
            return 0;
        } else if (this.isId()) {
            return -1;
        } else if (o.isId()) {
            return 1;
        }
        return 0;
    }

}
