/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.id;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jianggujin.dbfly.mybatis.constant.JKeyOrder;

/**
 * 优先级第四，生成 SQL，初始化时执行，优先级低于 sql
 * 
 * @author jianggujin
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JUseSqlGenerator {

    /**
     * 优先级第四，生成 SQL，初始化时执行，优先级低于 sql
     *
     * @return
     */
    Class<? extends JSqlGenerator> value();

    /**
     * 和 sql 可以配合使用
     *
     * @return
     */
    JKeyOrder order() default JKeyOrder.BEFORE;
}
