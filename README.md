# 第一部分 简介

`JDBFly`是一个基于`JAVA`的持久层开发框架，包含两部分内容：`Mybatis`增强、数据库版本跟踪。在简化常规开发的同时屏蔽数据库的差异，通过`JDBFly`使开发者更加关注业务本身，如雄鹰般在天空自由翱翔，从繁琐重复的持久层编码中解放出来。

## 1.1 特性

- **侵入小**：对`Mybatis`只做增强，对原有原生代码不会产生影响，仅需调整少量`JDBFly`配置代码
- **损耗小**：启动即会自动注入内置`Mapper`，提供基本` CRUD`，无额外性能损耗
- **强大的CRUD操作**：内置通用`Mapper`，仅仅通过少量配置即可实现单表大部分`CRUD`操作，更有强大的条件构造器，满足各类使用需求
- **支持多种条件查询**： 通过字符串或`Lambda`表达式，方便的编写各类查询条件，开发者可自由使用
- **多种主键策略**：支持`6`种主键策略（`JDBC自增主键`、`方言SQL`、`自定义SQL`、`动态自定义SQL`、`JAVA代码`、`主键标识`），可自由配置
- **支持`JPA`方法名称解析**：支持`JPA`形式通过方法名称定义`Mapper`方法
- **支持自定义Mapper扩展**：提供自定义`Mapper`入口，快速实现常用方法，并可根据实际业务需要自由组合内置`Mapper`
- **动态表名**：提供自定义动态表名方法，完美解决多租户分表需求
- **乐观锁**：内置乐观锁实现，提供并发处理的快速实现
- **自定义XML标签扩展**：开发者可自由扩展原生`Mybatis`标签，定制符合自身业务的标签，内置了常用多数据库适配的函数标签
- **多数据库适配**：支持`MySQL`、`MariaDB`、`Oracle`、`达梦` 等多种数据库，针对特殊数据库提供方言接口可有开发者扩展
- **多种集成方式**：支持原生`Java`项目、`Spring`、`SpringBoot`项目集成
- **数据库版本跟踪**：对`Flyway`进行扩展，自动适配数据库脚本，对数据库版本自动维护升级

## 1.2 支持数据库

标准功能部分支持所有标准数据库，全功能已经测试通过数据库有：`MySql`、`MariaDB `、`H2`、`Oracle`、`PostgreSQL`、`达梦`。 

## 1.3 实现原理

### 1.3.1 `Mybatis`增强

`JDBFly`提供了一些通用的方法，这些通用方法是以接口的形式提供的。接口和方法都使用了泛型，使用该通用方法的接口需要指定泛型的类型。通过`Java`反射可以得到接口泛型的类型信息，即对应实体类的信息。在实体类上通过特殊的注解完成实体类与数据库关系的映射。

得到了`Mapper`方法、实体类映射关系之后，通过生成符合`Mybatis`规范的`XML`，最终再交由`Mybatis`解析处理。默认情况下`JDBFly`不对`Mybatis`生成的`MappedStatement`进行修改，但是这个规则并不是强制的，开发者可以自定义构建器进行特殊的处理。`JDBFly`修改了`MapperAnnotationBuilder`默认行为，在`Mybatis`解析完`Mapper`接口以及关联的`XML`文件后，进行后置处理，增加自定义增强`Mapper`的扩展处理。

为了实现对`Mybatis`默认标签的扩展，`JDBFly`重写了`EntityResolver`，将静态`DTD`文件转换为`Java`实体，在运行时动态生成`DTD`规则，允许开发者扩展自己的节点解析器的时候修改`DTD`实体相关内容。

### 1.3.2 数据库版本跟踪

`JDBFly`对`flyway`进行了二次封装，扩展了针对不同数据库匹配相应脚本的方法。

## 1.4 安装

使用`JDbFly`可以直接下载源代码编译或者下载已经编译的`jar`文件，如果您是使用`maven`来构建项目，也可以直接在`pom.xml`中添加`JDBFly`的坐标：

[![Maven central](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JDBFly/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.jianggujin/JDBFly)

```xml
<!-- http://mvnrepository.com/artifact/com.jianggujin/JDBFly -->
<dependency>
    <groupId>com.jianggujin</groupId>
    <artifactId>JDBFly</artifactId>
    <version>最新版本</version>
</dependency>
```

最新的版本可以从[Maven仓库](http://mvnrepository.com/artifact/com.jianggujin/JDBFly)或者[码云](https://gitee.com/jianggujin/JDBFly)获取。

如果使用快照`SNAPSHOT`版本需要添加仓库，且版本号为快照版本 [点击查看最新快照版本号](https://oss.sonatype.org/content/repositories/snapshots/com/jianggujin/JDBFly/)

```xml
<repository>
    <id>snapshots</id>
    <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
</repository>
```



> 注意：引入 `JDBFly` 之后请不要再次引入 `MyBatis` 、 `MyBatis-Spring`、`mybatis-boot-starter`，以避免因版本差异导致的问题。

# 第二部分 `Mybatis`增强

## 2.1 快速开始

### 2.1.1 `JAVA`集成

#### 2.1.1.1 从`XML`中构建 `SqlSessionFactory`

每个基于`MyBatis`的应用都是以一个`SqlSessionFactory`的实例为核心的，`JDBFly`也同样如此。与直接使用`Mybatis`的区别在于`SqlSessionFactory`的实例的构建方式，`JDBFly`需要将`SqlSessionFactoryBuilder`替换为`JSqlSessionFactoryBuilder` 。

```java
String resource = "mybatis-config.xml";
InputStream inputStream = Resources.getResourceAsStream(resource);
SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
```

`XML`配置文件中包含了对`MyBatis`系统的核心设置，包括获取数据库连接实例的数据源（`DataSource`）以及决定事务作用域和控制方式的事务管理器（`TransactionManager`），示例如下：

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
  <environments default="development">
    <environment id="development">
      <transactionManager type="JDBC"/>
      <dataSource type="POOLED">
        <property name="driver" value="${driver}"/>
        <property name="url" value="${url}"/>
        <property name="username" value="${username}"/>
        <property name="password" value="${password}"/>
      </dataSource>
    </environment>
  </environments>
  <mappers>
    <package name="com.jianggujin.dbfly.test.mapper" />
  </mappers>
</configuration>
```

#### 2.1.1.2不使用`XML`构建`SqlSessionFactory`

除了使用`XML`创建配置之外，`JDBFly`也可以像`Mybatis`一样直接从`Java`代码创建。

```java
DataSource dataSource = BlogDataSourceFactory.getBlogDataSource();
TransactionFactory transactionFactory = new JdbcTransactionFactory();
Environment environment = new Environment("development", transactionFactory, dataSource);
Configuration configuration = new JConfiguration(environment);
configuration.addMappers("com.jianggujin.dbfly.mapper");
SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(configuration);
```

这里需要注意的是与`XML`创建配置不同的地方，除了需要将`SqlSessionFactoryBuilder`替换为`JSqlSessionFactoryBuilder`之外，还需要将`Configuration`替换为`JConfiguration`，实际上通过`XML`方式最终获得的配置类也是`JConfiguration`。

> `JSqlSessionFactoryBuilder`是`SqlSessionFactoryBuilder`的子类，`JConfiguration`是`Configuration`的子类，在`JDBFly`环境下获得的`Configuration`都应该是`JConfiguration`。

### 2.1.2 `Spring`集成

#### 2.1.2.1 通过`XML`配置

```xml
<bean id="sqlSessionFactory" class="com.jianggujin.dbfly.spring.JSqlSessionFactoryBean">
    <property name="dataSource" ref="dataSource"/>
</bean>
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="com.jianggujin.dbfly.test.mapper"/>
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

上面是默认的配置，在配置`sqlSessionFactory`时需要将`SqlSessionFactoryBean`替换为`JSqlSessionFactoryBean`，其他配置与直接使用`Mybatis`一样，有些时候需要一些特殊的配置，可能会需要自定义`Configuration`，这时候，一定要使用`JConfiguration`替换`Mybatis`的`Configuration`。

```xml
<bean id="configuration" class="com.jianggujin.dbfly.mybatis.JConfiguration"/>
<bean id="sqlSessionFactory" class="com.jianggujin.dbfly.spring.JSqlSessionFactoryBean">
    <property name="dataSource" ref="dataSource"/>
    <property name="configuration" ref="configuration"/>
</bean>
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="com.jianggujin.dbfly.test.mapper"/>
    <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
</bean>
```

#### 2.1.2.2 通过`@JMapperScan`注解配置

`Mybatis`官方提供的注解为`MapperScan`，在使用注解进行配置的时候，开发者需要使用`JDBFly`提供的`JMapperScan`替换官方注解。二者注解属性相同。

```java
@Configuration
@JMapperScan("com.jianggujin.dbfly.test.mapper")
public class AppConfig {
    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder().addScript("schema.sql").build();
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sessionFactory = new JSqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        return sessionFactory.getObject();
    }
}
```

### 2.1.3 `SpringBoot`集成

#### 2.1.3.1 自动装配

`JDBFly`支持`SpringBoot`自动装配，为了降低集成与迁移成本，`JDBFly`的自动装配参数与直接使用`mybatis-boot-starter`保持一致，只是在其基础上增加了`JDBFly`特有的配置，如果不需要特殊处理，则配置与`mybatis-boot-starter`相同。需要注意的是，如果没有结合`@JMapperScan`注解直接使用自动装配，自动装配会从`SpringBoot`基础包开始扫描，需要在`Mapper`接口上增加`@Mapper`注解，否则`MyBatis`无法判断扫描哪些接口。

例如在`properties`配置中：

```properties
mybatis.configuration.default-enum-type-handler=org.apache.ibatis.type.EnumOrdinalTypeHandler
mybatis.type-aliases-package=com.jianggujin.dbfly.test.entity
mybatis.dbfly.style=camelhump
```

> `mybatis.dbfly`开头的是`JDBFly`特有的配置

#### 2.1.2.2 通过`@JMapperScan`注解配置

在`SpringBoot`环境中同样可以使用`@JMapperScan`注解，可以与自动装配配合使用，在不使用该注解的情况下，`Mybatis`从基础包开始扫描，通过`@JMapperScan`注解可以限定扫描范围，提升扫描装配效率，也是比较推荐的用法。

## 2.2 对象关系映射

集成完`JDBFly`之后，需要做的事情就是将项目中的实体类与数据库中的表进行映射。在`JDBFly`中提供了实体与表映射的注解，此章节仅介绍常规的通用注解，对于有特殊功能的注解，比如主键策略、动态表名相关注解将在对应章节介绍。

比如在数据库中有人员信息表：

```mysql
CREATE TABLE user_info (
    id INt AUTO_INCREMENT COMMENT '主键',
    user_no VARCHAR(8) NOT NULL COMMENT '人员编号',
    user_name VARCHAR(8) NOT NULL COMMENT '人员姓名',
    PRIMARY KEY (id)
)
```

则该表对应的`Java`实体如下：

```java
Public class UserInfo {
    @JUseGeneratedKeys
    private Integer id;
    private String userNo;
    private String userName;
    // 省略gettre和setter
}
```

### `@JNameStyle`注解

描述：用于配置`Java`实体与数据库表、`Java`实体属性与数据库列之间名称的转换关系。

属性：

| 属性名  | 必填 |                             说明                             |
| :-----: | :--: | :----------------------------------------------------------: |
| `value` |  是  | 枚举类型`JStyle`，指定转换规则，<br />可选值如下：<br />`normal`：原值<br />`camelhump`：驼峰转下划线<br />`uppercase`：转换为大写<br />`lowercase`：转换为小写<br />`camelhumpAndUppercase`：驼峰转下划线大写形式<br />`camelhumpAndLowercase`：驼峰转下划线小写形式 |

示例：

```java
@JNameStyle(JStyle.camelhump)
private String userId; // => user_id
```

### `@JTable`注解

描述：用于指定`Java`实体对应的数据库表名称，该注解优先级最高，使用该注解之后`名称转换规则`与`动态表名`将失效，以`value`的值为准。

属性：

| 属性名  | 必填 |       说明       |
| :-----: | :--: | :--------------: |
| `value` |  是  | 对应数据库表名称 |
| `schema` |  否  | 可选schema |

示例：

```java
@JTable("TUC_USER") // TUC_USER
public class User {
    // ...
}
```

### `@JColumn`注解

描述：用于指定`Java`实体属性对应的数据库列名称，该注解优先级最高，使用该注解之后`名称转换规则`与将失效，以`value`的值为准。

属性：

| 属性名  | 必填 |       说明       |
| :-----: | :--: | :--------------: |
| `value` |  是  | 对应数据库列名称 |

示例：

```java
@JColumn("user_id")
private String userId; // => user_id
```

### `@JExcludeInsert`注解

描述：用于在插入操作的时候排除对应属性，即执行通用`Mapper`插入方法的时候，添加`@JExcludeInsert`注解的属性将会被忽略。

示例：

```java
@JExcludeInsert
private String userId;
```

### `@JExcludeSelect`注解

描述：用于在查询操作的时候排除对应属性，即执行通用`Mapper`查询方法的时候，添加`@JExcludeSelect`注解的属性将会被忽略。

示例：

```java
@JExcludeSelect
private String userId;
```

### `@JExcludeUpdate`注解

描述：用于在修改操作的时候排除对应属性，即执行通用`Mapper`修改方法的时候，添加`@JExcludeUpdate`注解的属性将会被忽略。

示例：

```java
@JExcludeUpdate
private String userId;
```

### `@JTransient`注解

描述：用于忽略`Java`实体属性，通常情况下用于在`Java`实体中冗余属性，但实际该属性没有对应的列，不需要处理，该注解与直接在属性上添加`transient`关键字效果是一样的。如果将该注解放在`Mapper`的方法上面，默认解析器将忽略该方法。

示例：

```java
@JTransient
private String userId;
```

### `@JJdbcType`注解

描述：用于指定`Java`实体属性对应的`Jdbc`类型，当某些情况`Java`实体属性无法被正确识别转换的时候需要添加该注解。

属性：

| 属性名  | 必填 |                             说明                             |
| :-----: | :--: | :----------------------------------------------------------: |
| `value` |  是  | 枚举类型`JdbcType`，<br />可选值如下：<br />`ARRAY`、`BIT`、`TINYINT`、`SMALLINT`、`INTEGER`<br/>`BIGINT`、`FLOAT`、`REAL`、`DOUBLE`、`NUMERIC`<br/>`DECIMAL`、`CHAR`、`VARCHAR`、`LONGVARCHAR`、`DATE`<br/>`TIME`、`TIMESTAMP`、`BINARY`、`VARBINARY`、`LONGVARBINARY`<br/>`NULL`、`OTHER`、`BLOB`、`CLOB`、`BOOLEAN`<br/>`CURSOR`、`UNDEFINED`、`NVARCHAR`、`NCHAR`、`NCLOB`<br/>`STRUCT`、`JAVA_OBJECT`、`DISTINCT`、`REF`、`DATALINK`<br/>`ROWID`、`LONGNVARCHAR`、`SQLXML`、`DATETIMEOFFSET` |

示例：

```java
@JJdbcType(JdbcType.VARCHAR)
private String userId;// => #{userId, jdbcType=VARCHAR}
```

### `@JTypeHandler`注解

描述：用于指定`Java`实体属性对应的`TypeHandler`，一般用于特殊类型的转换，比如`枚举`的处理。

属性：

| 属性名  | 必填 |        说明        |
| :-----: | :--: | :----------------: |
| `value` |  是  | 类型处理器的实现类 |

示例：

```java
@JTypeHandler(EnumOrdinalTypeHandler.class)
private TrueFalse isDeleted;// => #{userId, typeHandler=org.apache.ibatis.type.EnumOrdinalTypeHandler}
```

### `@JUseJavaType`注解

描述：用于告知通用`Mapper`，在处理该`Java`实体属性的时候是否添加`javaType`配置

示例：

```java
@JUseJavaType
private String userId; // => #{userId, javaType=java.lang.String}
```

### `@JUseNotEmpty`注解

描述：用于当字符串类型的`Java`实体属性作为条件时是否增加`!=''`的判断。

示例：

```java
@JUseNotEmpty
private String userId; // <if test="userId != null and userId != ''">...</if>
```

### `@JOrder`注解

描述：用于标注`Java`实体属性为排序属性，在通用`Mapper`进行默认的查询操作时会将其作为排序条件拼接在最终的`SQL`语句中。

属性：

|   属性名   | 必填 |                             说明                             |
| :--------: | :--: | :----------------------------------------------------------: |
|  `value`   |  否  | 枚举类型`JOrderStrategy`，指定排序的策略，<br />默认值为`ASC`，可选值如下：<br />`ASC`：升序<br />`DESC`：降序 |
| `priority` |  否  | 排序的优先级，默认值为`1`，<br />值越小，将会优先处理排在排序`SQL`的前面。 |

示例：

```java
@JOrder
private String userId; // => user_id asc
```

### `@JOverwriteGlobal`注解

描述：用于覆盖全局的配置，比如在`JDBFly`中可以指定排除某些`Java`实体属性在查询的时候不查询，但是在部分`Java`实体中存在特例，可以通过在对应实体上添加该注解以覆盖全局的配置。

属性：

| 属性名  | 必填 |              说明              |
| :-----: | :--: | :----------------------------: |
| `value` |  是  | 指定需要覆盖的配置对应的注解类 |

示例：

```java
@JOverwriteGlobal(JExcludeUpdate.class)
private String userId;
```

## 2.3 内置`Mapper`

`JDBFly`对常用的`CRUD`操作做了通用`Mapper`的封装，开发者只需要将自己的`Mapper`继承`JDBFly`内置的通用`Mapper`即可拥有相应的操作方法。比如有一个用户信息实体`UserInfo`，需要对该实体进行增删改查的操作，开发者只需要定义一个`Mapper`继承`JDBFly`通用`Mapper`即可。

```java
public interface UserInfoMapper extends JBaseMapper<UserInfo> {
}
```

`JBaseMapper`提供了常用操作的很多方法，有些时候并不需要这么多的操作，针对这种场景，开发者只需要按需选择继承相应的`Mapper`，同时`JDBFly`允许开发者自己编写`Mapper`方法与内置通用`Mapper`方法在一个`Mapper`中混合使用，默认情况下，开发者自己的方法优先级高于默认方法。

### 2.3.1 通用内置`Mapper`

按照具体的操作分类，内置通用`Mapper`分类如下：

- 插入类

  |          Mapper          |                           说明                           |
  | :----------------------: | :------------------------------------------------------: |
  |     `JInsertMapper`      | 保存一个实体，`null`的属性也会保存，不会使用数据库默认值 |
  | `JInsertSelectiveMapper` |  保存一个实体，`null`的属性不会保存，会使用数据库默认值  |

- 删除类

  |           Mapper           |                         说明                         |
  | :------------------------: | :--------------------------------------------------: |
  | `JDeleteByConditionMapper` |             根据`Condition`条件删除数据              |
  |    `JDeleteByIdMapper`     | 根据主键字段进行删除，方法参数必须包含完整的主键属性 |
  |      `JDeleteMapper`       |    根据实体属性作为条件进行删除，查询条件使用等号    |

- 修改类

  |               Mapper                |                      说明                       |
  | :---------------------------------: | :---------------------------------------------: |
  |     `JUpdateByConditionMapper`      |             根据`Condition`条件修改             |
  |         `JUpdateByIdMapper`         |   根据主键更新实体全部字段，`null`值会被更新    |
  | `JUpdateSelectiveByConditionMapper` | 根据`Condition`条件修改，仅会修改`!=null`的数据 |
  |    `JUpdateSelectiveByIdMapper`     |         根据主键更新属性不为`null`的值          |

- 查询类

  |             Mapper              |                             说明                             |
  | :-----------------------------: | :----------------------------------------------------------: |
  |   `JSelectByConditionMapper`    |                 根据`Condition`条件进行查询                  |
  |       `JSelectOneMapper`        | 根据实体中的属性进行查询，只能有一个返回值，<br />有多个结果是抛出异常，查询条件使用等号 |
  |  `JSelectOneByConditionMapper`  | 根据`Condition`条件进行查询，只能有一个返回值，<br />有多个结果是抛出异常 |
  |         `JSelectMapper`         |         根据实体中的属性值进行查询，查询条件使用等号         |
  |      `JSelectCountMapper`       |          根据实体中的属性查询总数，查询条件使用等号          |
  |     `JSelectCountAllMapper`     |                           查询总数                           |
  | `JSelectCountByConditionMapper` |               根据`Condition`条件进行查询总数                |
  |       `JSelectByIdMapper`       | 根据主键字段进行查询，<br />方法参数必须包含完整的主键属性，查询条件使用等号 |
  |       `JSelectAllMapper`        |                         查询全部结果                         |
  |         `JExistsMapper`         |      根据实体中的属性判断数据是否存在，查询条件使用等号      |
  |       `JExistsByIdMapper`       | 根据主键字段判断数据是否存在，<br />方法参数必须包含完整的主键属性，查询条件使用等号 |
  |   `JExistsByConditionMapper`    |             根据`Condition`条件判断数据是否存在              |

上面介绍的`Mapper`为最小的操作单位，`JDBFly`按照对应的操作分类提供了汇总的基础`Mapper`：`JBaseInsertMapper`、`JBaseDeleteMapper`、`JBaseUpdateMapper`、`JBaseSelectMapper`，分别对应了`插入`、`删除`、`修改`、`查询`的操作。在大部分情况下，开发者仅需要继承基础的`JBaseMapper`，该`Mapper`包含了所有通用的内置方法。

### 2.3.2 扩展内置`Mapper`

扩展内置`Mapper`无法保证通用性，开发者按需选择。

|                Mapper                 |                             说明                             |
| :-----------------------------------: | :----------------------------------------------------------: |
|          `JInsertListMapper`          | 批量保存实体，`null`的属性也会保存，<br />不会使用数据库默认值 |
|         `JDeleteByIdsMapper`          |                       根据主键集合删除                       |
|         `JSelectByIdsMapper`          |                       根据主键集合查询                       |
| `JSelectOneOptionalByConditionMapper` | 根据`Condition`条件进行查询，只能有一个返回值，<br />有多个结果是抛出异常，需要依赖`mybatis3.5+` |
|      `JSelectOneOptionalMapper`       | 根据实体中的属性进行查询，只能有一个返回值，<br />有多个结果是抛出异常，查询条件使用等号，<br />需要依赖`mybatis3.5+` |
|      `JSelectOptionalByIdMapper`      | 根据主键字段进行查询，<br />方法参数必须包含完整的主键属性，查询条件使用等号，<br />需要依赖`mybatis3.5+` |
|        `JSelectPageAllMapper`         |                    查询全部结果，支持分页                    |
|    `JSelectPageByConditionMapper`     |            根据`Condition`条件进行查询，支持分页             |
|          `JSelectPageMapper`          |    根据实体中的属性值进行查询，查询条件使用等号，支持分页    |
|           `JBasePageMapper`           |                    基础`Mapper`，包含分页                    |

## 2.4 方法名称解析

使用内置的通用`Mapper`，可以简化开发，但是有些时候也会有点麻烦，比如通过固定属性作为条件、查询部分属性等，为了进一步简化对数据库的操作，`JDBFly`支持像`JPA`那样通过方法名称定义相关的操作。如果需要开启方法名称解析，只需要让对应的`Mapper`继承`JMethodMapper`即可，当然`JBaseMapper`已经默认开启了方法名称解析。

```java
public interface UserInfoMapper extends JMethodMapper<UserInfo> {
    findUserIdAndUserNameByid(Integer id);
}
```

### 2.4.1 命名规则

使用方法名称解析功能，Mapper方法需要按照指定的命名规范才能被正常识别与解析，目前方法名称解析仅支持查询和删除，对应的方法名命名规则如下：

**查询**：`[(select|find|read|get|query)[Distinct][Exclude][selectProperties]By][whereProperties][OrderBy(orderProperties)]`

**删除**：`(delete|remove)By[whereProperties]`

各部分说明：

- **()**：表示必选项
- **[]**：表示可选项
- **Distinct**：存在该关键词将为查询语句添加`DISTINCT`
- **Exclude**：存在该关键词表示后面的查询属性为需要排除的
- **selectProperties**：该表达式表示需要查询的属性，不存在该部分则默认查询全部属性，属性首字母大写且属性与属性之间需要使用`And`分隔
- **whereProperties**：该表达式表示`where`条件需要使用的属性，分段条件之间通过`Or`或`And`拼接，`Or`优先级大于`And`，分段条件格式为:`peoperty[Keyword]`，属性首字母大写，需要注意的是对应方法形参数量必须与拆分后的条件参数数量一致
- **orderProperties**：该表达式表示`Order by`部分需要使用的属性，属性首字母大写，多个排序条件之间使用`Asc`或`Desc`分隔，如果该部分表达式不存在则是使用默认的排序配置

作为条件可用关键词：

|                           关键词                           |                             示例                             |         SQL代码段          |
| :--------------------------------------------------------: | :----------------------------------------------------------: | :------------------------: |
|                `IsNotNull`、<br />`NotNull`                |         `findByIdIsNotNull`、<br />`findByIdNotNull`         |      `id is not null`      |
|                   `IsNull`、<br />`Null`                   |            `findByIdIsNull`、<br />`findByIdNull`            |        `id is null`        |
|    `IsNot`、<br />`Not`、<br />`NotEquals`、<br />`Ne`     | `findByIdIsNot`、<br />`findByIdNot`、<br />`findByIdNotEquals`<br />`findByIdNe` |         `id <> ?1`         |
|              `Is`、<br />`Equals`、<br/>`Eq`               |    `findByIdIs`、<br />`findByIdEquals`<br />`findByIdEq`    |         `id = ?1`          |
|      `IsGreaterThan`、<br />`GreaterThan`、<br />`Gt`      | `findByIdIsGreaterThan`、<br />`findByIdGreaterThan`、<br />`findByIdGt` |         `id > ?1`          |
| `IsGreaterThanEqual`、<br />`GreaterThanEqual`、<br />`Ge` | `findByIdIsGreaterThanEqual`、<br />`findByIdGreaterThanEqual`、<br />`findByIdGe` |         `id >= ?1`         |
|         `IsLessThan`、<br />`LessThan`、<br />`Lt`         | `findByIdIsLessThan`、<br />`findByIdLessThan`、<br />`findByIdLt` |         `id < ?1`          |
|    `IsLessThanEqual`、<br />`LessThanEqual`、<br />`Le`    | `findByIdIsLessThanEqual`、<br />`findByIdLessThanEqual`、<br />`findByIdLe` |         `id <= ?1`         |
|                 `IsBefore`、<br />`Before`                 |          `findByIdIsBefore`、<br />`findByIdBefore`          |         `id < ?1`          |
|            `IsBeforeEqual`、<br />`BeforeEqual`            |     `findByIdIsBeforeEqual`、<br />`findByIdBeforeEqual`     |         `id <= ?1`         |
|                  `IsAfter`、<br />`After`                  |           `findByIdIsAfter`、<br />`findByIdAfter`           |         `id > ?1`          |
|             `IsAfterEqual`、<br />`AfterEqual`             |      `findByIdIsAfterEqual`、<br />`findByIdAfterEqual`      |         `id >= ?1`         |
|                  `IsNotIn`、<br />`NotIn`                  |           `findByIdIsNotIn`、<br />`findByIdNotIn`           |      `id not in (?1)`      |
|                     `IsIn`、<br />`In`                     |              `findByIdIsIn`、<br />`findByIdIn`              |        `id in (?1)`        |
|                `IsBetween`、<br />`Between`                |         `findByIdIsBetween`、<br />`findByIdBetween`         |   `id between ?1 and ?2`   |
|             `IsNotBetween`、<br />`NotBetween`             |      `findByIdIsNotBetween`、<br />`findByIdNotBetween`      | `id not between ?1 and ?2` |
|                   `IsLike`、<br />`Like`                   |            `findByIdIsLike`、<br />`findByIdLike`            |        `id like ?1`        |
|                `IsNotLike`、<br />`NotLike`                |         `findByIdIsNotLike`、<br />`findByIdNotLike`         |      `id not like ?1`      |

### 2.4.2 使用注解解析

在使用方法名称解析的时候，在某些情况下可能会导致方法名称冗长，可以在方法上面搭配注解来缩短方法名称。存在注解优先级大于方法名称解析结果且不同部分之间互不影响。

```java
public interface UserInfoMapper extends JMethodMapper<UserInfo> {
    @JProperty(exclude = { "id" })
    @JOrderBy(value = { @JOrderProperty(value = "userId", strategy = JOrderStrategy.DESC) })
    List<UserInfo> findAll();
}
```

#### `@JProperty`注解

描述：用于配置选择或排除的列对应的属性，等价于查询的`[(select|find|read|get|query)[Distinct][Exclude][selectProperties]By]`部分。

属性：

|  属性名   | 必填 |                             说明                             |
| :-------: | :--: | :----------------------------------------------------------: |
|  `value`  |  否  | 用于指定选择的列对应的属性，<br />优先级高于`exclude`，如果与`exclude`都为空则查询全部属性。 |
| `exclude` |  否  |                用于指定排除选择的列对应的属性                |

示例：

```java
@JProperty({"id", "userName"})
public User findByUserId(String userId);
```

#### `@JWhere`注解

描述：用于配置条件部分的属性以及条件，等价于查询或删除的`[whereProperties]`部分。

属性：

| 属性名  | 必填 |                  说明                  |
| :-----: | :--: | :------------------------------------: |
| `value` |  是  | 一组动态条件，属性值为`@JCriteria`注解 |

示例：

```java
@JWhere(@JCriteria({ @JCriterion("id"), @JCriterion("usrName") }))
User selectIdAndUserNameBy(Integer id, String userName);
```

#### `@JCriteria`注解

描述：用于配置一个动态条件的分组内容。

属性：

| 属性名  | 必填 |                说明                 |
| :-----: | :--: | :---------------------------------: |
| `value` |  是  | 动态条件，属性值为`@JCriterion`注解 |
| `isOr`  |  否  |           是否为`OR`条件            |

示例：

```java
@JWhere(@JCriteria({ @JCriterion("id"), @JCriterion("usrName") }))
User selectIdAndUserNameBy(Integer id, String userName);
```

#### `@JCriterion`注解

描述：用于配置一个动态条件。

属性：

| 属性名  | 必填 |                             说明                             |
| :-----: | :--: | :----------------------------------------------------------: |
| `value` |  是  |                           属性名称                           |
| `isOr`  |  否  |                        是否为`OR`条件                        |
| `type`  |  否  | 枚举类型`JType`，指定排序的策略，<br />默认值为`EQUALS`，可选值如下：<br />`IS_NOT_NULL`、`IS_NULL`、`NOT_EQUALS`<br/>`EQUALS`、`GREATER_THAN`、`GREATER_THAN_EQUAL`<br/>`LESS_THAN`、`LESS_THAN_EQUAL`、`BEFORE`<br/>`BEFORE_EQUAL`、`AFTER`、`AFTER_EQUAL`<br/>`NOT_IN`、`IN`、`NOT_BETWEEN`<br/>`BETWEEN`、`NOT_LIKE`、`LIKE` |

示例：

```java
@JWhere(@JCriteria({ @JCriterion("id"), @JCriterion("usrName") }))
User selectIdAndUserNameBy(Integer id, String userName);
```

#### `@JOrderBy`注解

描述：用于在查询的时候使用的排序条件，等价于查询的`[OrderBy(orderProperties)]`部分。

属性：

| 属性名  | 必填 |                  说明                   |
| :-----: | :--: | :-------------------------------------: |
| `value` |  是  | 排序属性，属性值为`@JOrderProperty`注解 |

示例：

```java
@JOrderBy(@JCriteria("id"))
User selectIdAndUserNameBy(Integer id, String userName);
```

#### `@JOrderProperty`注解

描述：用于配置排序属性以及排序策略。

属性：

|   属性名   | 必填 |                             说明                             |
| :--------: | :--: | :----------------------------------------------------------: |
|  `value`   |  是  |                           排序属性                           |
| `strategy` |  否  | 枚举类型`JOrderStrategy`，指定排序的策略，<br />默认值为`ASC`，可选值如下：<br />`ASC`：升序<br />`DESC`：降序 |

示例：

```java
@JOrderBy(@JCriteria("id"))
User selectIdAndUserNameBy(Integer id, String userName);
```

## 2.5 条件构造器

在`JDBFly`内置的通用`Mapper`中，以`ByCondition`结尾的方法允许开发者通过复杂的条件进行操作。

> `JDBFly`默认不支持`MBG`生成`Example`，如果需要可由开发者自行扩展

```java
JCondition condition = new JCondition(UserInfo.class);
condition.and().andEqualTo("id", 1);
List<UserInfo> list = mapper.selectByCondition(condition);
```

### 2.5.1 动态条件

通过`JCondition`对象的`and`或`or`方法可以获得`JCriteria`对象，利用该对象可以实现复杂的条件的动态组合。对应方法后缀效果如下：

| 方法后缀 | 参考SQL代码段 |
| :------: | :-------: |
| `IsNull` |`column is null`|
| `IsNotNull` |`column is not null`|
| `EqualTo`、`Et` |`column = value`|
| `NotEqualTo`、`Ne` |`column <> value`|
| `GreaterThan`、`Gt` |`column > value`|
| `GreaterThanOrEqualTo`、`Ge` |`column >= value`|
| `LessThan`、`Lt` |`column < value`|
| `LessThanOrEqualTo`、`Le` |`column <= value`|
| `In` |`column in (value)`|
| `NotIn` |`column not in (value)`|
| `Between` |`column between value1 and value2`|
| `NotBetween` |`column not between value1 and value2`|
| `Like` |`column like value`|
|  `LikeLeft`|`column like '%value'`|
|  `LikeRight`|`column like 'value%'`|
| `NotLike` |`column not like value`|
|  `NotLikeLeft`|`column not like '%value'`|
|  `NotLikeRight`|`column not like 'value%'`|
| `Condition` |手写条件，以实际为准|
| `EqualTo` |将不为空的字段参数作为相等查询条件|
| `AllEqualTo` |将所有字段参数作为相等查询条件，如果字段为`null`，则为`is null`|

`and`或`or`的前缀表示生成的条件使用`AND`或者`OR`，方法后缀规则一致，不再重复赘述。除了上述通过`and`或`or`前缀开头的方法构造动态条件以外，还可以通过更简单的链式调用，方法功能与上述方法后缀一致，仅需要将首字母变为小写，默认情况下链式调用的链接方式与获得条件对象时一致，如果需要更改，仅需要调用`and()`或`or()`方法切换当前链式调用状态。比如有如下条件：

```java
condition.and().andEqualTo("id", 1).andLikeRight("userName", "jiang");
```

可以简写为：

```java
condition.and().equalTo("id", 1).likeRight("userName", "jiang");
```

部分条件方法存在简化同义方法，比如上面的例子可以进一步简化为：

```java
condition.and().eq("id", 1).likeRight("userName", "jiang");
```

### 2.5.2 排序

```java
JCondition condition = new JCondition(UserInfo.class);
condition.orderBy("id").asc().orderBy("userId").desc();
mapper.selectByCondition(condition));
```

排序有两种方式，第一种：使用`JCondition`对象的`orderBy`方法，通过链式调用构建排序规则；第二种方式：使用`JCondition`对象的`setOrderBySql`方法直接设置排序`SQL`片段。

### 2.5.3 去重

```java
JCondition condition = new JCondition(UserInfo.class);
condition.setDistinct(true);
mapper.selectByCondition(condition));
```

去重需要调用`JCondition`对象的`setDistinct`方法并设置为`true`。

### 2.5.4 排他锁

```java
JCondition condition = new JCondition(UserInfo.class);
condition.setForUpdate(true);
mapper.selectByCondition(condition));
```

排他锁需要调用`JCondition`对象的`setsetForUpdate`方法并设置为`true`。

### 2.5.5 查询或排除列

```java
JCondition condition = new JCondition(UserInfo.class);
condition.selectProperties("id", "userId");
mapper.selectByCondition(condition));
```

`JCondition`中提供查询属性和排除属性的方法，分别为`selectProperties`和`excludeProperties`。查询属性优先级高于排除属性，如果不设置，则默认查询全部属性。

### 2.5.6 `Lambda`形式调用

```java
JLambdaCondition<UserInfo> condition = new JLambdaCondition<>(UserInfo.class);
condition.lambdaAnd().andEqualTo(UserInfo::getId, 1);
mapper.selectByCondition(condition);
```

`JCondition`为开发者提供了灵活的设置方式，但是通过字符串形式编写条件，容易出现编写错误、或者实体类升级之后，属性修改不同步出现一些错误，`JDBFly`同时提供了`JLambdaCondition`，可以通过`lambda`表达式定义属性，将错误提升到编译阶段，提前暴露问题，`JLambdaCondition`同时提供了通过`lambda`调用以及`JCondition`的所有能力，使用更为灵活。

## 2.6 主键策略

`JDBFly`中提供了`6`种主键策略，可由开发者自由组合使用。每种主键策略对应一个注解，多个注解联合使用时优先选择优先级别较高的注解配置，忽略优先级别低的注解。

### `@JUseGeneratedKeys`注解

描述：第一优先级主键策略，表示是否使用`JDBC`方式获取主键，其效果等价于在`XML`文件中使用`useGeneratedKeys="true"`的形式，同一个实体类中只允许在一个属性上使用该注解。

示例：

```java
@JUseGeneratedKeys
private Integer id;
```

### `@JUseIdentityDialect`注解

描述：第二优先级主键策略，根据方言配置的数据库类型取回主键，其效果等价于在`XML`文件中使用`selectKey`，同一个实体类中只允许在一个属性上使用该注解。

示例：

以内置的`MySQL`方言为例，假设有如下实例配置：

```java
Public class UserInfo {
    @JUseIdentityDialect
    private Integer id;
    private String userNo;
    private String userName;
    // 省略gettre和setter
}
```

则会生成如下`XML`片段：

```xml
<insert>
    <selectKey keyProperty="id" order="AFTER" resultType="int">
        SELECT LAST_INSERT_ID()
    </selectKey>
</insert>
```

### `@JUseSql`注解

描述：第三优先级主键策略，根据配置的获取主键的`SQL`与主键生成顺序获取主键，其效果等价于在`XML`文件中使用`selectKey`，同一个实体类中只允许在一个属性上使用该注解。

属性：

| 属性名  | 必填 |                             说明                             |
| :-----: | :--: | :----------------------------------------------------------: |
| `value` |  是  |                        取主键的`SQL`                         |
| `order` |  否  | 枚举类型`JKeyOrder`，指定主键生成顺序，<br />默认值为`BEFORE`，可选值如下：<br />`AFTER`：`insert`后执行`SQL`<br />`BEFORE`：`insert`前执行`SQL` |

示例：

```java
@JUseSql(value = "SELECT LAST_INSERT_ID()", order = JKeyOrder.AFTER)
private Integer id;
```

### `@JUseSqlGenerator`注解

描述：第四优先级主键策略，结合`SQL`生成器`JSqlGenerator`动态生成获取主键的`SQL`与主键生成顺序获取主键，其效果等价于在`XML`文件中使用`selectKey`，同一个实体类中只允许在一个属性上使用该注解。

属性：

| 属性名  | 必填 |                             说明                             |
| :-----: | :--: | :----------------------------------------------------------: |
| `value` |  是  |                      `SQL`生成器实现类                       |
| `order` |  否  | 枚举类型`JKeyOrder`，指定主键生成顺序，<br />默认值为`BEFORE`，可选值如下：<br />`AFTER`：`insert`后执行`SQL`<br />`BEFORE`：`insert`前执行`SQL` |

示例：

```java
@JUseSqlGenerator(IdSqlGenerator.class)
private Integer id;
```

### `@JUseIdGenerator`注解

描述：第五优先级主键策略，通过`Java`方式生成主键，需要和主键生成器`JIdGenerator`结合使用，执行插入操作之前会自动将生成的主键注入到对应实体中。

属性：

| 属性名  | 必填 |       说明       |
| :-----: | :--: | :--------------: |
| `value` |  是  | 主键生成器实现类 |

示例：

```java
@JUseSqlGenerator(IdGenerator.class)
private Integer id;
```

### `@JId`注解

描述：第六优先级主键策略，仅仅标记当前属性为主键，具体操作由开发者通过代码控制。

示例：

```java
@JId
private String userId;
```

## 2.7 动态表名

在多租户的场景下，可能会需要按照租户进行分表，`JDBFly`允许开发者动态指定表名，前文中的`@JTable`注解用于指定静态表名，当需要动态表名处理的时候需要使用`@JUseDynamicTableName`注解。需要注意的是`@JUseDynamicTableName`注解不能与`@JTable`注解同时使用，否则动态表名不生效。

`JDBFly`中默认提供了一种动态表名实现`JDynamicTableNameGenerator`，需要实体类实现`JDynamicTableName`接口，在对该实体进行数据库操作时，会将表名修改为实际返回的表名。

### `@JUseDynamicTableName`注解

描述：用于配置指定实体使用动态表名。

属性：

| 属性名  | 必填 |                 说明                 |
| :-----: | :--: | :----------------------------------: |
| `value` |  是  |        动态表名生成器的实现类        |
| `name`  |  否  | 默认的表名，等价于使用静态表名的形式 |
| `schema` |  否  | 可选schema |

示例：

```java
@JUseDynamicTableName(JDynamicTableNameGenerator.class)
public class User {
    // ...
}
```

### 2.8 乐观锁

当要修改一条记录的时候，希望与数据库记录同步没有被其他人修改的时候，可以使用`JDBFly`提供的乐观锁功能。开发者仅需要在开乐观锁的属性上增加`@JVersion`注解。在使用`JDBFly`内置方法进行修改操作时，会自动设置新的版本号，需要注意的是在使用乐观锁功能时不应该将乐观锁属性排除查询。

`JDBFly`内置了一种默认的乐观锁版本号生成器实现`JDefaultVersionGenerator`，支持的数据类型有：`Integer`、`Long`、`Timestamp`、`Date`。`Integer`、`Long`会在原有数据基础上加`1`，`Timestamp`、`Date`会取当前时间。

### `@JVersion`注解

描述：版本号，用于乐观锁，一个实体类中只能存在一个，不能与主键策略一起使用，优先级高于`JGeneratedValue`。

属性：

| 属性名  | 必填 |        说明        |
| :-----: | :--: | :----------------: |
| `value` |  是  | 版本号生成器实现类 |

示例：

```java
@JVersion
private Integer version;
```

## 2.9 自动生成属性值

通常情况下，创建数据库表的时候会冗余创建时间与最后一次修改时间，对应再进行相关插入、修改操作的时候需要手动设置相关时间的数据，`JDBFly`提供了`@JGeneratedValue`注解用于自动生成指定属性的数据。需要注意的是`@JGeneratedValue`注解与主键策略注解或`@JVersion`注解共同使用无效，仅会在`insert`、`update`操作下生效，且受是否允许添加与修改限制。

`JDBFly`内置了一种默认的日期类型数据的生成器实现`JDateValueGenerator`，支持的数据类型有：`Date`、`java.sql.Timestamp`、`java.sql.Date`、`java.sql.Time`、`String`，使用`String`数据类型的时候需要结合`@JFormat`注解指定格式化模板，默认模板为：`yyyyMMddHHmmss`。

### `@JGeneratedValue`注解

描述：自动生成指定属性的数据，与主键策略注解共同使用无效，仅会在`insert`、`update`操作下生效，且受是否允许添加与修改限制。

属性：

| 属性名  | 必填 |       说明       |
| :-----: | :--: | :--------------: |
| `value` |  是  | 数据生成器实现类 |

示例：

```java
@JGeneratedValue(JDateValueGenerator.class)
private Date createTime;
```

## 2.10 自定义`Mapper`扩展

当`JDBFly`内置通用`Mapper`不满足开发需求的时候，开发者可以按照规范自定义自己业务相关的`Mapper`。这里我们用一个内置的`Mapper`来介绍自定义`Mapper`扩展的步骤，本质上是一样的，只不过内置`Mapper`是已经写好的，仅此而已。

### 2.10.1 编写`Mapper`接口

我们以`JSelectAllMapper`为例，首先我们需要创建`JSelectAllMapper`接口，并增加泛型参数，形如：

```java
public interface JSelectAllMapper<T> {
}
```

一般情况下，建议开发者在一个Mapper接口中只做一件事情，接下来就需要在刚刚创建的接口中添加对应的Mapper方法。

```java
public interface JSelectAllMapper<T> {
    @JMappedStatementProvider(type = JBaseSelectProvider.class)
    List<T> selectAll();
}
```

可以看到在定义的`selectAll`方法上使用了`@JMappedStatementProvider`注解，该注解用于指定生成此方法`XML`片段的服务实现类与对应实现方法。`@JMappedStatementProvider`注解包含两个属性`type`和`method`，`type`用于指定生成此方法`XML`片段的服务实现类；`method`用于指定对应的实现方法，该方法可以是静态方法，也可以是实例方法，但是方法形参必须是`(JConfiguration configuration, Class<?> mapperClass, Method method, Document document, Element mapperElement)`，否则无法解析，`method`属性的默认值为`dynamicMethod`，表示动态映射方法，比如例子中服务实现类是`JBaseSelectProvider`，`Mapper`方法名称为`selectAll`，那么对应的实现方法就是`JBaseSelectProvider#selectAll(JConfiguration configuration, Class<?> mapperClass, Method method, Document document, Element mapperElement)`，若不使用动态映射方法，则可以指定方法名称。

### 2.10.2 编写`XML`片段实现方法

有了`Mapper`接口与实现方法的映射关系，接下来就是编写真正的实现方法。

```java
public class JBaseSelectProvider {
    public void selectAll(JConfiguration configuration, Class<?> mapperClass, Method method, Document document,
            Element mapperElement) {
        JEntity entity = JProviderHelper.resolveEntity(configuration, mapperClass, method);
        String resultMap = JProviderHelper.checkAndCreateResultMap(configuration, document, mapperElement, entity);
        Element selectElement = JElementBuilder.buildSelectElement(document, method, resultMap, null);
        JProviderHelper.selectAllColumns(document, selectElement, entity);
        JProviderHelper.fromTable(configuration, document, selectElement, entity);
        JProviderHelper.orderByDefault(document, selectElement, entity);
        mapperElement.appendChild(selectElement);
    }
}
```

实现方法的本质上就是通过`Java`的`Dom`方式编辑`Mapper`对应的`XML`，开发者只需要按照方法生成对应的`Element`之后将其添加到根元素`mapperElement`中即可。

## 2.11 自定义`MappedStatement`构造器

`JDBFly`内置的构造器支持XML与方法名称的解析，如果实际业务有更加个性化的需求，或者开发者想提供自己的解析方式，`JDBFly`支持自定义的`JMappedStatementBuilder`，不过并不建议这样做。

### 2.11.1 编写构造器实现

```java
public interface JMappedStatementBuilder {
    /**
     * 解析
     * 
     * @param configuration
     * @param mapperClass
     */
    void parse(JConfiguration configuration, Class<?> mapperClass);

    /**
     * 判断是否支持
     * 
     * @param configuration
     * @param mapperClass
     * @return
     */
    boolean support(JConfiguration configuration, Class<?> mapperClass);
}
```

自定义`MappedStatement`构造器，开发者需要实现`JMappedStatementBuilder`接口，并重写`parse`与`support`方法，`JDBFly`在解析`Mapper`接口时会从`MappedStatement构造器注册器`获取所有已注册的`MappedStatement`构造器，通过`support`方法验证构造器是否支持对应`Mapper`接口，如支持则会执行`parse`真正的解析，即使`Mapper`接口被构造器解析之后，当查找到后续构造器依然支持解析该`Mapper`，会再次解析，建议开发者除非有必要对`MappedStatement`对象进行二次处理，否则应该先判断是否有未解析的方法，避免多构造器重复处理。

```java
String namespace = mapperClass.getName();
if (configuration.isResourceLoaded(namespace)) {
    return;
}
Method[] methods = JMybatisUtils.filterUnParseMethods(configuration, mapperClass);
if (methods == null || methods.length == 0) {
    return;
}
```

### 2.11.2 注册构造器实现

开发完自己的构造器之后，需要将构造器加入`JDBFly`的配置`JDBFlyConfiguration`中，通过调用`JDBFlyConfiguration`的`getMappedStatementBuilderRegistry()`方法可以获得构造器的注册器，通过注册器的`addMappedStatementBuilder`添加自定义的构造器，构造器名称默认取实现类的简单类名，出现同名时后添加的构造器将覆盖已有构造器。

## 2.12 自定义`XML`标签扩展

自定义`XML`标签用于`Mybatis`提供的默认标签不满足开发需要的场景，比如在`JDBFly`中，为了屏蔽数据库的差异，对于一些常用函数进行了`XML`标签的扩展，这部分标签全部以`fn_`开头，开发者只需要使用内置的扩展标签即可实现不同数据库的差异屏蔽，当然，`JDBFly`无法穷举所有的实际开发场景，所以有类似需求的时候，开发者可以通过`JDBFly`进行原生`Mybatis`标签的扩展。

### 2.12.1 编写节点解析器

开发者需要实现`JNodeHandler`接口来开发自定义节点解析器，该接口有一个需要实现的方法`handleNode`和一个`accept`默认方法。

```java
public interface JNodeHandler extends Consumer<JDTD> {
    void handleNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    @Override
    default void accept(JDTD dtd) {
    }
}
```

`handleNode`方法用于解析标签并生成相应的`SqlNode`，以原生的`bind`标签的解析器为例：

```java
public class JBindHandler implements JNodeHandler {

    @Override
    public void handleNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        final String name = nodeToHandle.getStringAttribute("name");
        final String expression = nodeToHandle.getStringAttribute("value");
        final VarDeclSqlNode node = new VarDeclSqlNode(name, expression);
        targetContents.add(node);
    }
}
```

`JBindHandlerder`的实现与`Mybatis`官方的是一致的，只是类名与实现接口改变，通过`JDBFly`进行自定义`XML`标签扩展与`Mybatis`的内置实现相同，通过解析标签属性与内容生成`SqlNode`，开发者也可以根据实际情况创建自己的`SqlNode`。

`accept`方法用于对`DTD`的验证，因为`JDBFly`扩展了`XML`标签，在解析`XML`的时候会对`XML`文档的合法性进行校验，而自定义标签是`Mybatis`无法识别的，所以在`JDBFly`中将静态的`DTD`文件转变为`Java`对象，可以动态进行修改，当开发者扩展了`XML`标签之后，需要实现`accept`方法以添加或修改为符合实际节点的规则。

```java
public class JFnHandler implements JNodeHandler {

    @Override
    public void handleNode(JXMLScriptBuilder builder, XNode nodeToHandle,
        List<SqlNode> targetContents) {
        JConfiguration configuration = (JConfiguration) builder.getConfiguration();
        JDialect dialect = configuration.getDBFlyConfiguration().getDialect();
        if (!dialect.handleFnNode(builder, nodeToHandle, targetContents)) {
            throw new JDBFlyException("无法解析{}函数标签", nodeToHandle.getName());
        }
    }

    @Override
    public void accept(JDTD dtd) {
        JDTDItem empty = new JDTDEmpty();
        JDTDPCData PCDATA = new JDTDPCData();
        /****** 字符串函数 ******/
        // 参数第一个字符的ASCII码
        dtd.add(new JDTDElement("fn_ascii", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 字符串拼接
        dtd.add(new JDTDElement("fn_concat", new JDTDMixed(new JDTDName("value", JDTDCardinal.ONEMANY))));
        // 返回字符串的字符数
        dtd.add(new JDTDElement("fn_char_length", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 返回字符串的字节数
        dtd.add(new JDTDElement("fn_bit_length", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 返回指定的子字串的位置
        dtd.add(new JDTDElement("fn_position", empty, new JDTDAttribute("sub", "CDATA"),
                new JDTDAttribute("str", "CDATA")));
        // 把字串str里出现地所有子字串from替换成子字串to
        dtd.add(new JDTDElement("fn_replace", empty, new JDTDAttribute("str", "CDATA"),
                new JDTDAttribute("from", "CDATA"), new JDTDAttribute("to", "CDATA")));
        // 从字符串str的start位置截取长度为length的子字符串
        dtd.add(new JDTDElement("fn_sub_str", empty, new JDTDAttribute("str", "CDATA"),
                new JDTDAttribute("start", "CDATA"), new JDTDAttribute("length", "CDATA")));
        // 将字符串转换为大写
        dtd.add(new JDTDElement("fn_upper", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 将字符串转换为小写
        dtd.add(new JDTDElement("fn_lower", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 去掉字符串开始和结尾处的空格
        dtd.add(new JDTDElement("fn_trim", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 去掉字符串开始处的空格
        dtd.add(new JDTDElement("fn_ltrim", PCDATA, new JDTDAttribute("str", "CDATA")));
        // 去掉字符串结尾处的空格
        dtd.add(new JDTDElement("fn_rtrim", PCDATA, new JDTDAttribute("str", "CDATA")));
        /****** 数字函数 ******/
        /****** 日期函数 ******/
        /****** 类型转换函数 ******/
        // 日期格式化
        dtd.add(new JDTDElement("fn_date_format", PCDATA, new JDTDAttribute("format", "CDATA", JDTDDecl.REQUIRED),
                new JDTDAttribute("date", "CDATA")));
        // 字符串转日期
        dtd.add(new JDTDElement("fn_str_to_date", PCDATA, new JDTDAttribute("format", "CDATA", JDTDDecl.REQUIRED),
                new JDTDAttribute("str", "CDATA")));
        /****** 其他函数 ******/
        dtd.add(new JDTDElement("fn_nvl", empty, new JDTDAttribute("exp1", "CDATA", JDTDDecl.REQUIRED),
                new JDTDAttribute("exp2", "CDATA", JDTDDecl.REQUIRED)));
        /****** 辅助标签 ******/
        // 用于分隔参数
        dtd.add(new JDTDElement("value", PCDATA));
        String[] names = { "select", "insert", "selectKey", "update", "delete", "sql", "trim", "where", "set",
                "foreach", "when", "otherwise", "if" };
        for (String name : names) {
            JDTDElement element = dtd.get(name);
            JDTDMixed mixed = (JDTDMixed) element.getContent();
            mixed.add("fn_ascii", "fn_concat", "fn_char_length", "fn_bit_length", "fn_position", "fn_replace",
                    "fn_sub_str", "fn_upper", "fn_lower", "fn_trim", "fn_ltrim", "fn_rtrim", "fn_date_format",
                    "fn_str_to_date", "fn_nvl");
        }
    }
}
```

### 2.12.2 注册节点解析器

开发完自己的节点解析器之后，需要将节点解析器设置到`JXMLScriptBuilder`中，通过调用`JXMLScriptBuilder`的`setNodeHandler`方法可以将节点解析器加入到脚本构建器中，同时`JDBFly`也提供了`SPI`扩展的形式进行注入，开发者可以在`META-INF/services/com.jianggujin.dbfly.mybatis.xmltag.JNodeHandler`文件中添加解析器实现类。因为节点解析器是针对节点进行解析的，所以在设置节点解析器的时候需要告诉`JDBFly`解析器可以解析的标签名称，有两种实现方式：

第一种：节点解析器实现`JCustomNodeHandler`接口的`nodeNames`方法，返回值为可以解析的`XML`标签名称数组。

第二种：取节点解析器的简单类名，如果名称以`Handler`结尾将自动截断，并将类名首字母小写作为可以解析的XML标签名称，如：`If -> if`、`IfHandler -> if`。

需要注意的是如果多个节点解析器同时支持一个`XML`标签，后添加的会覆盖之前添加的。

### 2.12.3 开发阶段验证XML文档

自定义的`XML`标签在运行阶段解析的时候需要动态生成，防止解析验证失败，同样的，开发好的自定义标签在开发阶段编辑`XML`的时候，开发工具默认会提供辅助操作，比如智能提示、错误验证等，这时候自定义的标签会出现问题，虽然不影响最终程序运行，但是总会让开发者心情不美丽，这时候可以通过生成新的`DTD`文件，通过指定`URI`的方式将`XML`验证的`DTD`文件指向自己修改后的文件，避免开发工具直接从网络获取官方`DTD`文件。开发者可通过如下代码生成最终的修改后的`DTD`文件。

```java
JDTD dtd = new JDBFlyMapperDTDFactory().createDTD();
dtd.write(new PrintWriter(new FileWriter("mybatis-3-mapper.dtd")));
```

> 如果使用`XML`方式配置`Mybatis`也可以通过该种方式，只需要将`JDBFlyMapperDTDFactory`替换为`JDBFlyConfigDTDFactory`即可

### 2.12.4 内置扩展标签

#### 2.12.4.1 函数类

##### 2.12.4.1.1 字符串函数

###### `fn_ascii`

描述：返回字符串第一个字符的`ASCII`码。

属性：

| 属性名 | 必填 |                      说明                      |
| :----: | :--: | :--------------------------------------------: |
| `str`  |  否  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_ascii>'jianggujin'</fn_ascii> <!-- 106 -->
<fn_ascii str="'jianggujin'"/> <!-- 106 -->
```

###### `fn_concat`

描述：字符串拼接，内部包含`value`标签，一个标签表示一个待拼接字符串，应至少包含两个非空`value`标签。

示例：

```xml
<fn_concat><value>'jiang'</value><value>'gujin'</value></fn_concat>
<!-- 'jianggujin' -->
```

###### `fn_char_length`

描述：返回字符串的字符数。

属性：

| 属性名 | 必填 |                      说明                      |
| :----: | :--: | :--------------------------------------------: |
| `str`  |  否  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_char_length>'jianggujin'</fn_char_length> <!-- 10 -->
<fn_char_length str="'jianggujin'"/> <!-- 10 -->
```

###### `fn_bit_length`

描述：返回字符串的字节数。

属性：

| 属性名 | 必填 |                      说明                      |
| :----: | :--: | :--------------------------------------------: |
| `str`  |  否  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_bit_length>'jianggujin'</fn_bit_length> <!-- 10 -->
<fn_bit_length str="'jianggujin'"/> <!-- 10 -->
```

######  `fn_position`

描述：返回字符串在目标字符串中的位置。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `sub`  |  是  |  子字符串内容  |
| `str`  |  是  | 目标字符串内容 |

示例：

```xml
<fn_position sub=“gu” str=“jianggujin”/> <!-- 6 -->
```

######  `fn_replace`

描述：把字符串`str`里出现地所有子字符串`from`替换成子字符串`to`。

属性：

| 属性名 | 必填 |       说明       |
| :----: | :--: | :--------------: |
| `str`  |  是  |     源字符串     |
| `from` |  是  | 需要替换的字符串 |
|  `to`  |  是  | 替换的结果字符串 |

示例：

```xml
<fn_replace str=“jianooujin” from="oo" to="gg"/> <!-- 'jianggujin' -->
```

######  `fn_sub_str`

描述：从字符串`str`的`start`位置截取长度为`length`的子字符串。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `str`  |  是  | 待截取的字符串 |
| `start`  |  是  | 开始位置 |
| `length  |  是  | 截取长度 |

示例

```xml
<fn_sub_str str=“jianggujin” start="6" length="2"/> <!-- 'gu' -->
```

######   `fn_upper`

描述：将字符串转换为大写。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_upper>'jianggujin'</fn_upper> <!-- 'JIANGGUJIN' -->
<fn_upper str="'jianggujin'"/> <!-- 'JIANGGUJIN' -->
```

######   `fn_lower`

描述：将字符串转换为小写。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_lower>'JIANGGUJIN'</fn_lower> <!-- 'jianggujin' -->
<fn_lower str="'JIANGGUJIN'"/> <!-- 'jianggujin' -->
```

######   `fn_trim`

描述：去掉字符串开始和结尾处的空格。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_trim>' jianggujin '</fn_trim> <!-- 'jianggujin' -->
<fn_trim str="' jianggujin '"/> <!-- 'jianggujin' -->
```

######   `fn_ltrim`

描述：去掉字符串开始和结尾处的空格。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_ltrim>' jianggujin '</fn_ltrim> <!-- 'jianggujin ' -->
<fn_ltrim str="' jianggujin '"/> <!-- 'jianggujin ' -->
```

######   `fn_rtrim`

描述：去掉字符串结尾处的空格。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_rtrim>' jianggujin '</fn_rtrim> <!-- ' jianggujin' -->
<fn_rtrim str="' jianggujin '"/> <!-- ' jianggujin' -->
```

##### 2.12.4.1.2 数字函数

##### 2.12.4.1.3 日期函数

##### 2.12.4.1.4 类型转换函数

######   `fn_date_format`

描述：日期格式化。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `format`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_date_format format="yyyyMMddHHmmss">Now()</fn_date_format>
<fn_date_format format="yyyyMMddHHmmss" date="Now()"/>
```


######   `fn_str_to_date`

描述：字符串转日期

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `format`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_str_to_date format="yyyyMMddHHmmss">'20201106144233'</fn_str_to_date>
<fn_str_to_date format="yyyyMMddHHmmss" str="'20201106144233'"/>
```

##### 2.12.4.1.2 其他函数


######   `fn_nvl`

描述：如果`exp1`的值不为`NULL`，则返回`exp1`，否则返回`exp2`。

属性：

| 属性名 | 必填 |      说明      |
| :----: | :--: | :------------: |
| `format`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |
| `str`  |  是  | 字符串内容，属性与元素文本二选一，优先元素文本 |

示例：

```xml
<fn_nvl exp1="amount" exp2="0"/>
```

#### 2.12.4.2 操作类

##### `page`

描述：分页标签

属性：

|   属性名   | 必填 |                             说明                             |
| :--------: | :--: | :----------------------------------------------------------: |
|   `name`   |  否  | 分页数据对象名称，如果该属性为空，<br />自动从参数中查找类型为`JPage`的对象，且其他属性配置无效，<br />如果该属性设置为：`$root`，等价于设置：`_parameter` |
| `startRow` |  否  |             指定开始行属性名称，默认：`startRow`             |
|  `endRow`  |  否  |              指定结束行属性名称，默认：`endRow`              |
| `pageSize` |  否  |           指定每页数据量属性名称，默认：`pageSize`           |

示例：

```xml
<page>select id from test</page>
<page name="page">select id from test</page>
```

#####   `alias`

描述：别名，自动添加`AS alias`，屏蔽不同数据库别名差异，例如`Oracle`数据库会自动为其添加双引号。

属性：

|  属性名  | 必填 |                      说明                      |
| :------: | :--: | :--------------------------------------------: |
| `name` |  否  | 别名，默认取节点内容 |
|  `as`   |  否  | 是否添加`AS`关键字，默认`true`，可选值如下：<br />`true`：添加<br />`false`：不添加 |

示例：

```xml
<alias>name</alias> // => name AS name
```

## 2.13 自定义方言扩展

方言是`JDBFly`中比较重要的部分，主要用于屏蔽数据库差异，提供针对不同数据库的个性化处理。

### 2.13.1 编写方言实现

编写自己的方言需要实现`JDialect`接口，对接口中相关方法进行扩展实现即可。

```java
public interface JDialect {

    /**
     * 查询或生成主键的SQL以及生成顺序，如不支持则返回null
     * 
     * @return
     */
    JIdentity getIdentity();

    /**
     * 处理函数节点， 返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     * @return
     */
    boolean handleFnNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理分页节点， 返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param builder
     * @param nodeToHandle
     * @param targetContents
     * @return
     */
    boolean handlePageNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents);

    /**
     * 处理批量插入，返回{@code true}表示支持并处理，{@code false}表示不支持
     * 
     * @param configuration
     * @param mapperClass
     * @param method
     * @param document
     * @param mapperElement
     * @param entity
     * @return
     */
    boolean handleInsertListMapperMethod(JConfiguration configuration, Class<?> mapperClass, Method method,
            Document document, Element mapperElement, JEntity entity);

    /**
     * 判断当前方言是否支持指定数据库元数据
     * 
     * @return
     * @throws SQLException
     */
    boolean support(DatabaseMetaData metaData) throws SQLException;

}

```

正常情况下，对方言的扩展实现建议开发者直接继承`JAbstarctDialect`，在`JAbstarctDialect`抽象化类中提供了一些通用的处理。

### 2.13.2 注册方言实现

开发完自己的方言之后，需要将方言加入`JDBFly`的配置`JDBFlyConfiguration`中，通过调用`JDBFlyConfiguration`的`getDialectRegistry()`方法可以获得方言的注册器，通过注册器的`addDialect`添加自定义的方言实现，方言名称默认取实现类的简单类名，出现同名时后添加的方言将覆盖已有方言。

除了通过方言注册器添加方言之外，开发者还可以直接通过`JDBFlyConfiguration`的`setDialect`方法设置指定的方言实例，二者区别在于直接设置的方言实例将作为后续`JDBFly`直接使用的方言，否则将通过注册器查找支持的方言实现。

## 2.14 `JDBFly`配置说明

`JDBFly`的特殊配置都是通过`JDBFlyConfiguration`进行配置，该配置实例可以通过`JConfiguration`类的`getDBFlyConfiguration()`方法获得。在`JDBFlyConfiguration`中可以通过配置的`set`方法进行设置或者通过`Properties`的形式进行配置，下面将介绍可以使用的配置属性以及说明。

描述：用于配置`Java`实体与数据库表、`Java`实体属性与数据库列之间名称的转换关系。

属性：

|      属性名      | 必填 |                             说明                             |
| :--------------: | :--: | :----------------------------------------------------------: |
|    `dialect`     |  否  |            指定方言实现类，会自动初始化为方言实例            |
|    `dialects`    |  否  | 需要注册的方言实现类，多个类之间使用`,`分隔，<br />会将其注册到方言注册器中 |
|     `style`      |  否  | 枚举类型`JStyle`，表名、属性名列名转换风格，<br />默认`camelhump`，可选值如下：<br />`normal`：原值<br />`camelhump`：驼峰转下划线<br />`uppercase`：转换为大写<br />`lowercase`：转换为小写<br />`camelhumpAndUppercase`：驼峰转下划线大写形式<br />`camelhumpAndLowercase`：驼峰转下划线小写形式 |
|  `useJavaType`   |  否  |               是否设置`javaType`，默认`false`                |
|    `notEmpty`    |  否  | 是否增加空字符串`!=''`判断，<br />仅当`Java`类型为字符串时生效，默认`false` |
| `exludeSelects`  |  否  |    全局需要排除的不需要查询的列，多个属性之间使用`,`分隔     |
| `excludeInserts` |  否  |    全局需要排除的不需要插入的列，多个属性之间使用`,`分隔     |
| `excludeUpdates` |  否  |    全局需要排除的不需要修改的列，多个属性之间使用`,`分隔     |
| `valueGeneratorProperties` |  否  |    全局`Value`生成器的列，多个属性之间使用`,`分隔     |
| `valueGeneratorClass` |  否  |    全局`Value`生成器实现类     |
| `schema` |  否  |    全局`schema`     |

> JDBFly的特殊配置除了可以通过`JDBFlyConfiguration`配置，在`JConfiguration`中也提供了快速的配置方法，二者是等价的。

# 第三部分 数据库版本跟踪


