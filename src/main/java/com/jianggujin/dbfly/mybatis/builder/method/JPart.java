/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder.method;

import com.jianggujin.dbfly.util.JAssert;

/**
 * 属性以及条件
 * 
 * @author jianggujin
 *
 */
public class JPart {

    /**
     * 属性
     */
    private final String property;
    /**
     * 条件类型
     */
    private final JType type;

    public JPart(String source) {
        JAssert.hasText(source, "Part source must not be null or empty!");
        this.type = JType.fromProperty(source);
        this.property = type.extractProperty(source);
    }

    /**
     * 获得所需参数数量
     * 
     * @return
     */
    public int getNumberOfArguments() {
        return type.getNumberOfArguments();
    }

    /**
     * 获得属性名称
     * 
     * @return
     */
    public String getProperty() {
        return property;
    }

    /**
     * 获得条件类型
     * 
     * @return
     */
    public JType getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("%s %s", property, type);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((property == null) ? 0 : property.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JPart other = (JPart) obj;
        if (property == null) {
            if (other.property != null)
                return false;
        } else if (!property.equals(other.property))
            return false;
        if (type != other.type)
            return false;
        return true;
    }
}
