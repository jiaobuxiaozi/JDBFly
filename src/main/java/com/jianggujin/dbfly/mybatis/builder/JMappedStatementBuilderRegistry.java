/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder;

import org.apache.ibatis.mapping.MappedStatement;

import com.jianggujin.dbfly.mybatis.JConfiguration;
import com.jianggujin.dbfly.util.JLinkedEntry;
import com.jianggujin.dbfly.util.JLinkedList;

/**
 * {@link MappedStatement}构造器注册器
 * 
 * @author jianggujin
 *
 */
public class JMappedStatementBuilderRegistry {
    private final JLinkedList<String, JMappedStatementBuilder> mappedStatementBuilders = new JLinkedList<>();

    public JMappedStatementBuilderRegistry() {
        this.mappedStatementBuilders.addLast("defaultMappedStatementBuilder", new JDefaultMappedStatementBuilder());
    }

    /**
     * 解析
     * 
     * @param configuration
     * @param type
     */
    public void parse(JConfiguration configuration, Class<?> type) {
        for (JLinkedEntry<String, JMappedStatementBuilder> mappedStatementBuilder : mappedStatementBuilders) {
            if (mappedStatementBuilder.getValue().support(configuration, type)) {
                mappedStatementBuilder.getValue().parse(configuration, type);
            }
        }
    }

    public JLinkedList<String, JMappedStatementBuilder> getMappedStatementBuilders() {
        return mappedStatementBuilders;
    }
}
