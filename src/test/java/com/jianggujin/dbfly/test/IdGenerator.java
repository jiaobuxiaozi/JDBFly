package com.jianggujin.dbfly.test;

import java.util.Random;

import com.jianggujin.dbfly.mybatis.entity.JEntity;
import com.jianggujin.dbfly.mybatis.entity.JEntityIdColumn;
import com.jianggujin.dbfly.mybatis.id.JIdGenerator;

public class IdGenerator implements JIdGenerator {

    @Override
    public Object generateId(JEntity entity, JEntityIdColumn idColumn) {
        return new Random().nextInt(200);
    }

}
