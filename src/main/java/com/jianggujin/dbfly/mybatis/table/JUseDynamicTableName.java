/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.table;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 是否使用动态表名
 * 
 * @author jianggujin
 *
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface JUseDynamicTableName {
    /**
     * 动态表名生成实现类
     * 
     * @return
     */
    Class<? extends JTableNameGenerator> value();

    /**
     * 默认表名
     * 
     * @return
     */
    String name() default "";

    /**
     * 可选schema
     */
    String schema() default "";

}
