package com.jianggujin.dbfly.test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jianggujin.dbfly.mybatis.JSqlSessionFactoryBuilder;
import com.jianggujin.dbfly.test.entity.TestEntityAuto;
import com.jianggujin.dbfly.test.mapper.InsertListMapper;

public class InsertListMapperTest {
    private SqlSession session = null;

    @Before
    public void init() throws Exception {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new JSqlSessionFactoryBuilder().build(inputStream);
        session = sqlSessionFactory.openSession();
    }

    @Test
    public void insertList() {
        List<TestEntityAuto> recordList = new ArrayList<>();
        TestEntityAuto testEntityAuto = new TestEntityAuto();
        testEntityAuto.setCode("11");
        testEntityAuto.setVersion(1);
        recordList.add(testEntityAuto);
        testEntityAuto = new TestEntityAuto();
        testEntityAuto.setCode("11");
        testEntityAuto.setVersion(1);
        recordList.add(testEntityAuto);
        System.err.println(session.getMapper(InsertListMapper.class).insertList(recordList));
    }

    @After
    public void destory() {
        if (session != null) {
            session.close();
        }
    }
}
