/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import java.lang.reflect.Field;

import com.jianggujin.dbfly.mybatis.constant.JIdGeneratorStrategy;
import com.jianggujin.dbfly.mybatis.constant.JKeyOrder;
import com.jianggujin.dbfly.mybatis.id.JIdGenerator;

/**
 * 主键实体列
 * 
 * @author jianggujin
 *
 */
public class JEntityIdColumn extends JEntityColumn {

    /**
     * 主键生成策略
     */
    private JIdGeneratorStrategy idGeneratorStrategy;
    /**
     * 主键生成SQL语句
     */
    private String idGeneratorSql;
    /**
     * 主键生成策略实现类
     */
    private Class<? extends JIdGenerator> idGeneratorClass;
    /**
     * 主键生成顺序
     */
    private JKeyOrder keyOrder;

    protected JEntityIdColumn(JEntity entity, Field field) {
        super(entity, field);
    }

    /**
     * 设置主键生成策略
     * 
     * @param idGeneratorStrategy
     */
    protected void setIdGeneratorStrategy(JIdGeneratorStrategy idGeneratorStrategy) {
        this.idGeneratorStrategy = idGeneratorStrategy;
    }

    /**
     * 设置主键生成SQL语句
     * 
     * @param idGeneratorSql
     */
    protected void setIdGeneratorSql(String idGeneratorSql) {
        this.idGeneratorSql = idGeneratorSql;
    }

    /**
     * 设置主键生成策略实现类
     * 
     * @param idGeneratorClass
     */
    protected void setIdGeneratorClass(Class<? extends JIdGenerator> idGeneratorClass) {
        this.idGeneratorClass = idGeneratorClass;
    }

    /**
     * 设置主键生成顺序
     * 
     * @param keyOrder
     */
    protected void setKeyOrder(JKeyOrder keyOrder) {
        this.keyOrder = keyOrder;
    }

    /**
     * 获得主键生成策略
     * 
     * @return
     */
    public JIdGeneratorStrategy getIdGeneratorStrategy() {
        return idGeneratorStrategy;
    }

    /**
     * 获得主键生成SQL语句
     * 
     * @return
     */
    public String getIdGeneratorSql() {
        return idGeneratorSql;
    }

    /**
     * 获得主键生成策略实现类
     * 
     * @return
     */
    public Class<? extends JIdGenerator> getIdGeneratorClass() {
        return idGeneratorClass;
    }

    /**
     * 获得主键生成顺序
     * 
     * @return
     */
    public JKeyOrder getKeyOrder() {
        return keyOrder;
    }
}
