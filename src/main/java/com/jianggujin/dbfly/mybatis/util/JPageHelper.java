/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.util;

import java.util.List;

import com.jianggujin.dbfly.mybatis.builder.additional.JBasePageMapper;
import com.jianggujin.dbfly.mybatis.builder.additional.JSelectPageAllMapper;
import com.jianggujin.dbfly.mybatis.builder.additional.JSelectPageByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.additional.JSelectPageMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectCountAllMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectCountByConditionMapper;
import com.jianggujin.dbfly.mybatis.builder.mapper.select.JSelectCountMapper;
import com.jianggujin.dbfly.mybatis.entity.JCondition;
import com.jianggujin.dbfly.mybatis.entity.JPage;
import com.jianggujin.dbfly.mybatis.entity.JPageData;

/**
 * 分页辅助工具
 * 
 * @author jianggujin
 *
 */
public class JPageHelper {
    public static <T> JPageData<T> selectPageData(T record, JSelectPageMapper<T> mapper, int pageNum, int pageSize,
            int total) {
        JPageData<T> page = new JPageData<>(pageNum, pageSize, total);
        page.setData(mapper.selectPage(record, page));
        return page;
    }

    public static <T> JPageData<T> selectPageData(T record, JBasePageMapper<T> mapper, int pageNum, int pageSize) {
        return selectPageData(record, mapper, pageNum, pageSize, mapper.selectCount(record));
    }

    public static <T> JPageData<T> selectPageData(T record, JSelectPageMapper<T> selectPageMapper,
            JSelectCountMapper<T> selectCountMapper, int pageNum, int pageSize) {
        return selectPageData(record, selectPageMapper, pageNum, pageSize, selectCountMapper.selectCount(record));
    }

    public static <T> JPageData<T> selectPageData(T record, JSelectPageMapper<T> mapper, JPage page) {
        JPageData<T> pageData = new JPageData<>(page);
        pageData.setData(mapper.selectPage(record, page));
        return pageData;
    }

    public static <T> JPageData<T> selectPageData(JCondition<T> condition, JSelectPageByConditionMapper<T> mapper,
            int pageNum, int pageSize, int total) {
        JPageData<T> page = new JPageData<>(pageNum, pageSize, total);
        page.setData(mapper.selectPageByCondition(condition, page));
        return page;
    }

    public static <T> JPageData<T> selectPageData(JCondition<T> condition, JBasePageMapper<T> mapper, int pageNum,
            int pageSize) {
        return selectPageData(condition, mapper, pageNum, pageSize, mapper.selectCountByCondition(condition));
    }

    public static <T> JPageData<T> selectPageData(JCondition<T> condition,
            JSelectPageByConditionMapper<T> selectPageByConditionMapper,
            JSelectCountByConditionMapper<T> selectCountByConditionMapper, int pageNum, int pageSize) {
        return selectPageData(condition, selectPageByConditionMapper, pageNum, pageSize,
                selectCountByConditionMapper.selectCountByCondition(condition));
    }

    public static <T> JPageData<T> selectPageData(JCondition<T> condition, JSelectPageByConditionMapper<T> mapper,
            JPage page) {
        JPageData<T> pageData = new JPageData<>(page);
        pageData.setData(mapper.selectPageByCondition(condition, page));
        return pageData;
    }

    public static <T> JPageData<T> selectPageData(JSelectPageAllMapper<T> mapper, int pageNum, int pageSize,
            int total) {
        JPageData<T> page = new JPageData<>(pageNum, pageSize, total);
        page.setData(mapper.selectPageAll(page));
        return page;
    }

    public static <T> JPageData<T> selectPageData(JBasePageMapper<T> mapper, int pageNum, int pageSize) {
        return selectPageData(mapper, pageNum, pageSize, mapper.selectCountAll());
    }

    public static <T> JPageData<T> selectPageData(JSelectPageAllMapper<T> selectPageAllMapper,
            JSelectCountAllMapper<T> selectCountAllMapper, int pageNum, int pageSize) {
        return selectPageData(selectPageAllMapper, pageNum, pageSize, selectCountAllMapper.selectCountAll());
    }

    public static <T> JPageData<T> selectPageData(JSelectPageAllMapper<T> mapper, JPage page) {
        JPageData<T> pageData = new JPageData<>(page);
        pageData.setData(mapper.selectPageAll(page));
        return pageData;
    }

    public static <T> List<T> selectPage(T record, JSelectPageMapper<T> mapper, int pageNum, int pageSize, int total) {
        return mapper.selectPage(record, new JPage(pageNum, pageSize, total));
    }

    public static <T> List<T> selectPage(T record, JBasePageMapper<T> mapper, int pageNum, int pageSize) {
        return selectPage(record, mapper, pageNum, pageSize, mapper.selectCount(record));
    }

    public static <T> List<T> selectPage(T record, JSelectPageMapper<T> selectPageMapper,
            JSelectCountMapper<T> selectCountMapper, int pageNum, int pageSize) {
        return selectPage(record, selectPageMapper, pageNum, pageSize, selectCountMapper.selectCount(record));
    }

    public static <T> List<T> selectPage(T record, JSelectPageMapper<T> mapper, JPage page) {
        return mapper.selectPage(record, page);
    }

    public static <T> List<T> selectPage(JCondition<T> condition, JSelectPageByConditionMapper<T> mapper, int pageNum,
            int pageSize, int total) {
        return mapper.selectPageByCondition(condition, new JPage(pageNum, pageSize, total));
    }

    public static <T> List<T> selectPage(JCondition<T> condition, JBasePageMapper<T> mapper, int pageNum,
            int pageSize) {
        return selectPage(condition, mapper, pageNum, pageSize, mapper.selectCountByCondition(condition));
    }

    public static <T> List<T> selectPage(JCondition<T> condition,
            JSelectPageByConditionMapper<T> selectPageByConditionMapper,
            JSelectCountByConditionMapper<T> selectCountByConditionMapper, int pageNum, int pageSize) {
        return selectPage(condition, selectPageByConditionMapper, pageNum, pageSize,
                selectCountByConditionMapper.selectCountByCondition(condition));
    }

    public static <T> List<T> selectPage(JCondition<T> condition, JSelectPageByConditionMapper<T> mapper, JPage page) {
        return mapper.selectPageByCondition(condition, page);
    }

    public static <T> List<T> selectPage(JSelectPageAllMapper<T> mapper, int pageNum, int pageSize, int total) {
        return mapper.selectPageAll(new JPage(pageNum, pageSize, total));
    }

    public static <T> List<T> selectPage(JBasePageMapper<T> mapper, int pageNum, int pageSize) {
        return selectPage(mapper, pageNum, pageSize, mapper.selectCountAll());
    }

    public static <T> List<T> selectPage(JSelectPageAllMapper<T> selectPageAllMapper,
            JSelectCountAllMapper<T> selectCountAllMapper, int pageNum, int pageSize) {
        return selectPage(selectPageAllMapper, pageNum, pageSize, selectCountAllMapper.selectCountAll());
    }

    public static <T> List<T> selectPage(JSelectPageAllMapper<T> mapper, JPage page) {
        return mapper.selectPageAll(page);
    }

}
