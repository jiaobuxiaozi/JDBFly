/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.entity;

import com.jianggujin.dbfly.mybatis.util.JLambdaPropertyUtils;
import com.jianggujin.dbfly.mybatis.util.JPropertyFunction;

/**
 * lambda查询条件
 * 
 * @author jianggujin
 *
 */
public class JLambdaCondition<T> extends JCondition<T> {

    /**
     * 默认exists为true
     *
     * @param entityClass
     */
    public JLambdaCondition(Class<T> entityClass) {
        super(entityClass);
    }

    /**
     * 带exists参数的构造方法，默认notNull为false，允许为空
     *
     * @param entityClass
     * @param exists      - true时，如果字段不存在就抛出异常，false时，如果不存在就不使用该字段的条件
     */
    public JLambdaCondition(Class<T> entityClass, boolean exists) {
        super(entityClass, exists);
    }

    /**
     * 带exists参数的构造方法
     *
     * @param entityClass
     * @param exists      - true时，如果字段不存在就抛出异常，false时，如果不存在就不使用该字段的条件
     * @param notNull     - true时，如果值为空，就会抛出异常，false时，如果为空就不使用该字段的条件
     */
    public JLambdaCondition(Class<T> entityClass, boolean exists, boolean notNull) {
        super(entityClass, exists, notNull);
    }

    /**
     * 排序
     * 
     * @param function
     * @return
     */
    public <R> JLambdaOrderBy<T> lambdaOrderBy(JPropertyFunction<T, R> function) {
        return new JLambdaOrderBy<>(this.orderBy(JLambdaPropertyUtils.resolve(function)));
    }

    /**
     * 排除查询字段，优先级低于selectPropertie
     * 
     * @param <R>
     * @param function
     * @return
     */
    public <R> JLambdaCondition<T> lambdaExcludePropertie(JPropertyFunction<T, R> function) {
        this.excludeProperties(JLambdaPropertyUtils.resolve(function));
        return this;
    }

    /**
     * 排除查询字段，优先级低于selectPropertie
     * 
     * @param <R>
     * @param function
     * @return
     */
    public <R> JLambdaCondition<T> lep(JPropertyFunction<T, R> function) {
        this.lambdaExcludePropertie(function);
        return this;
    }

    /**
     * 指定要查询的属性列
     * 
     * @param <U>
     * @param function
     * @return
     */
    public <U> JLambdaCondition<T> lambdaSelectPropertie(JPropertyFunction<T, U> function) {
        this.selectProperties(JLambdaPropertyUtils.resolve(function));
        return this;
    }

    /**
     * 指定要查询的属性列
     * @param <U>
     * @param function
     * @return
     */
    public <U> JLambdaCondition<T> lsp(JPropertyFunction<T, U> function) {
        return this.lambdaSelectPropertie(function);
    }

    public JLambdaCriteria<T> lambdaOr() {
        return new JLambdaCriteria<>(this.or());
    }

    public JLambdaCriteria<T> lambdaAnd() {
        return new JLambdaCriteria<>(this.and());
    }

    /**
     * 指定 count(property) 查询属性
     * 
     * @param function
     */
    public <R> void lambdaCountProperty(JPropertyFunction<T, R> function) {
        this.setCountProperty(JLambdaPropertyUtils.resolve(function));
    }
}
