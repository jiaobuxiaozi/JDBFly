/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis;

import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;

import com.jianggujin.dbfly.mybatis.dialect.JDialect;
import com.jianggujin.dbfly.mybatis.value.JValueGenerator;
import com.jianggujin.dbfly.util.JBanner;
import com.jianggujin.dbfly.util.JBeanUtils;
import com.jianggujin.dbfly.util.JStyle;

/**
 * 配置，继承自Mybatis的{@link Configuration}
 * 
 * @author jianggujin
 *
 */
public class JConfiguration extends Configuration {
    private final JDBFlyConfiguration dbFlyConfiguration;

    public JConfiguration(Environment environment) {
        this();
        this.environment = environment;
    }

    public JConfiguration() {
        new JBanner().printMybatisBanner();
        Map<String, Class<?>> typeAlias = JBeanUtils.getValue(this.typeAliasRegistry, "TYPE_ALIASES");
        typeAlias.put("XML", JXMLLanguageDriver.class);
        languageRegistry.setDefaultDriverClass(JXMLLanguageDriver.class);
        JBeanUtils.setValue(this, "mapperRegistry", new JMapperRegistry(this));
        this.dbFlyConfiguration = new JDBFlyConfiguration(this);
    }

    @Override
    public void setDefaultScriptingLanguage(Class<?> driver) {
        if (driver == null) {
            driver = JXMLLanguageDriver.class;
        }
        getLanguageRegistry().setDefaultDriverClass(driver);
    }

    public JDBFlyConfiguration getDBFlyConfiguration() {
        return dbFlyConfiguration;
    }

    public void setDialect(JDialect dialect) {
        this.dbFlyConfiguration.setDialect(dialect);
    }

    public void setExcludeSelects(String[] excludeSelects) {
        this.dbFlyConfiguration.setExcludeSelects(excludeSelects);
    }

    public void setExcludeInserts(String[] excludeInserts) {
        this.dbFlyConfiguration.setExcludeInserts(excludeInserts);
    }

    public void setExcludeUpdates(String[] excludeUpdates) {
        this.dbFlyConfiguration.setExcludeUpdates(excludeUpdates);
    }

    public void setValueGeneratorProperties(String[] valueGeneratorProperties) {
        this.dbFlyConfiguration.setValueGeneratorProperties(valueGeneratorProperties);
    }

    public void setValueGeneratorClass(Class<? extends JValueGenerator> valueGeneratorClass) {
        this.dbFlyConfiguration.setValueGeneratorClass(valueGeneratorClass);
    }

    public void setStyle(JStyle style) {
        this.dbFlyConfiguration.setStyle(style);
    }

    public void setSchema(String schema) {
        this.dbFlyConfiguration.setSchema(schema);
    }

    public void setUseJavaType(boolean useJavaType) {
        this.dbFlyConfiguration.setUseJavaType(useJavaType);
    }

    public void setNotEmpty(boolean notEmpty) {
        this.dbFlyConfiguration.setNotEmpty(notEmpty);
    }

    public void setProperties(Properties properties) {
        this.dbFlyConfiguration.setProperties(properties);
    }
}
