/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.Locale;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.jianggujin.dbfly.util.dtd.JDTD;

public class JXMLMapperEntityResolver implements EntityResolver {
    private static final String IBATIS_CONFIG_SYSTEM = "ibatis-3-config.dtd";
    private static final String IBATIS_MAPPER_SYSTEM = "ibatis-3-mapper.dtd";
    private static final String MYBATIS_CONFIG_SYSTEM = "mybatis-3-config.dtd";
    private static final String MYBATIS_MAPPER_SYSTEM = "mybatis-3-mapper.dtd";

    private JDBFlyDTDFactory mapperDTDFactory;
    private JDBFlyDTDFactory configDTDFactory;

    public JXMLMapperEntityResolver() {
        mapperDTDFactory = new JDBFlyMapperDTDFactory();
        configDTDFactory = new JDBFlyConfigDTDFactory();
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException {
        try {
            if (systemId != null) {
                String lowerCaseSystemId = systemId.toLowerCase(Locale.ENGLISH);
                if (lowerCaseSystemId.contains(MYBATIS_CONFIG_SYSTEM)
                        || lowerCaseSystemId.contains(IBATIS_CONFIG_SYSTEM)) {
                    return getInputSource(configDTDFactory, publicId, systemId);
                } else if (lowerCaseSystemId.contains(MYBATIS_MAPPER_SYSTEM)
                        || lowerCaseSystemId.contains(IBATIS_MAPPER_SYSTEM)) {
                    return getInputSource(mapperDTDFactory, publicId, systemId);
                }
            }
            return null;
        } catch (Exception e) {
            throw new SAXException(e.toString());
        }
    }

    private InputSource getInputSource(JDBFlyDTDFactory factory, String publicId, String systemId) {
        InputSource source = null;
        try {
            JDTD dtd = factory.createDTD();
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            dtd.write(new PrintWriter(out));
            source = new InputSource(new ByteArrayInputStream(out.toByteArray()));
            source.setPublicId(publicId);
            source.setSystemId(systemId);
        } catch (IOException e) {
            // ignore, null is ok
            source = new InputSource(new StringReader(""));
        }
        return source;
    }
}
