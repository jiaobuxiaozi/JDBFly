package com.jianggujin.dbfly.test.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.jianggujin.dbfly.mybatis.entity.JPage;
import com.jianggujin.dbfly.test.entity.TestEntity;

public interface FnMapper {
    String selectNow1();

    String selectNow2();

    Date selectNow3();

    Date selectNow4();

    int testNull();

    String testConcat();

    List<TestEntity> selectPage(@Param("page") JPage page);
}
