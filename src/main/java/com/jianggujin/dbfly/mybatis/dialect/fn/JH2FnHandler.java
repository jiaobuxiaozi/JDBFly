/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.dialect.fn;

import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;

import com.jianggujin.dbfly.mybatis.JXMLScriptBuilder;
import com.jianggujin.dbfly.mybatis.util.JNodeUtils;

/**
 * 扩展的函数处理器H2实现
 * 
 * @author jianggujin
 *
 */
public class JH2FnHandler extends JDefaultFnHandler {

    /**
     * 处理char_length，返回字符串的字符数，使用LENGTH
     */
    @Override
    public boolean handleFnCharLengthNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" LENGTH(").append(JNodeUtils.getRequiredValue(nodeToHandle, "str"))
                .append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnDateFormatNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" FORMATDATETIME(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "date")).append(", '")
                .append(nodeToHandle.getStringAttribute("format")).append("') ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnStrToDateNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" PARSEDATETIME(")
                .append(JNodeUtils.getRequiredValue(nodeToHandle, "str")).append(", '")
                .append(nodeToHandle.getStringAttribute("format")).append("') ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

    @Override
    public boolean handleFnNvlNode(JXMLScriptBuilder builder, XNode nodeToHandle, List<SqlNode> targetContents) {
        StringBuilder builder2 = new StringBuilder(" NVL2(").append(JNodeUtils.getRequiredValue(nodeToHandle, "exp1"))
                .append(", ").append(JNodeUtils.getRequiredValue(nodeToHandle, "exp2")).append(") ");
        JNodeUtils.addSqlNode(builder2, targetContents);
        return true;
    }

}
