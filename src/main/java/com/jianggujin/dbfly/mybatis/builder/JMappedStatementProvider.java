/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.mybatis.builder;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import org.apache.ibatis.mapping.MappedStatement;

/**
 * {@link MappedStatement}构造器服务
 * 
 * @author jianggujin
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface JMappedStatementProvider {
    /**
     * 构造器服务类
     * 
     * @return
     */
    Class<?> type();

    /**
     * 服务方法
     * 
     * @return
     */
    String method() default JDefaultMappedStatementBuilder.DYNAMIC_METHOD;

}
