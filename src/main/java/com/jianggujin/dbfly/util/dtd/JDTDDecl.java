/**
 * Copyright 2020 jianggujin (www.jianggujin.com).
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jianggujin.dbfly.util.dtd;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * 元素属性值
 * @author jianggujin
 *
 */
public enum JDTDDecl implements JDTDOutput {
    /**
     * 属性值是固定的
     */
    FIXED() {
        @Override
        public void write(PrintWriter out) throws IOException {
            out.print(" #FIXED");
        }
    },
    /**
     * 属性值是必需的
     */
    REQUIRED() {
        @Override
        public void write(PrintWriter out) throws IOException {
            out.print(" #REQUIRED");
        }
    },
    /**
     * 属性不是必需的
     */
    IMPLIED() {
        @Override
        public void write(PrintWriter out) throws IOException {
            out.print(" #IMPLIED");
        }
    },
    /**
     * 属性的默认值
     */
    VALUE() {
        @Override
        public void write(PrintWriter out) throws IOException {
        }
    }
}
