package com.jianggujin.dbfly.mybatis.util;

import java.util.List;

import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.scripting.xmltags.SqlNode;
import org.apache.ibatis.scripting.xmltags.StaticTextSqlNode;
import org.apache.ibatis.scripting.xmltags.TextSqlNode;

import com.jianggujin.dbfly.util.JDBFlyException;
import com.jianggujin.dbfly.util.JStringUtils;

/**
 * XML节点工具
 * 
 * @author jianggujin
 *
 */
public class JNodeUtils {
    /**
     * 添加SqlNode
     * 
     * @param sequence
     * @param targetContents
     */
    public static void addSqlNode(CharSequence sequence, List<SqlNode> targetContents) {
        String data = sequence.toString();
        TextSqlNode textSqlNode = new TextSqlNode(data);
        if (textSqlNode.isDynamic()) {
            targetContents.add(textSqlNode);
        } else {
            targetContents.add(new StaticTextSqlNode(data));
        }
    }

    /**
     * 获得必须的值，先获取节点数据，如果为空，从指定属性取值
     * 
     * @param node
     * @param attrName
     * @return
     */
    public static String getRequiredValue(XNode node, String attrName) {
        String value = node.getStringBody();
        if (JStringUtils.isEmpty(value)) {
            value = node.getStringAttribute(attrName);
        }
        if (JStringUtils.isEmpty(value)) {
            throw new JDBFlyException("{}节点不合法，属性{}或节点内容不能同时为空", node.getName(), attrName);
        }
        return value;
    }

    /**
     * 获得必须的属性值
     * 
     * @param node
     * @param attrName
     * @return
     */
    public static String getRequiredAttribute(XNode node, String attrName) {
        String value = node.getStringAttribute(attrName);
        if (JStringUtils.isEmpty(value)) {
            throw new JDBFlyException("{}节点不合法，属性{}值不能为空", node.getName(), attrName);
        }
        return value;
    }

    /**
     * 获得必须的节点数据
     * 
     * @param node
     * @return
     */
    public static String getRequiredBody(XNode node) {
        String value = node.getStringBody();
        if (JStringUtils.isEmpty(value)) {
            throw new JDBFlyException("{}节点不合法，节点内容不能为空", node.getName());
        }
        return value;
    }

}
