package com.jianggujin.dbfly.test.mapper;

import com.jianggujin.dbfly.mybatis.builder.additional.JInsertListMapper;
import com.jianggujin.dbfly.test.entity.TestEntityAuto;

public interface InsertListMapper extends JInsertListMapper<TestEntityAuto> {
}
